obj-m := femodule.o
ccflags-y := -std=gnu99 -Wno-declaration-after-statement -Wno-unused-function
# Flag options are -D_LION, -D__LYNX, -D__LYNX_SB, and -D__LYNX_EXT_CLK_10MHZ.
# The LYNX_SB variant is for the Radiolynx device but using the single-bit
# quantization bit-packing. This distinction is required because proper
# timestamping of the IMU measurements requires ``samples per front-end byte''
# to be known.
#
# Use -D__LYNX_EXT_CLK_10MHZ in conjunction with either -D__LYNX or -D__LYNX_SB
# to specify that an external 10MHz clock source is provided.
#
# RadioLion support through -D__LION is still a work in progress.
#
# -D__DEBUG_PROTECT enables certain checks for programming errors
# -D__DEBUG_DEVICE enables certain checks for proper device operation
ccflags-y += -D__LION -D__DEBUG_PROTECT #-D__LYNX_EXT_CLK_10MHZ #-D__LION

SOURCEDIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
HEADERSDIR := $(shell uname -r)

all:
	make -C /lib/modules/$(HEADERSDIR)/build M=$(SOURCEDIR) modules

install:
	make -C /lib/modules/$(HEADERSDIR)/build M=$(SOURCEDIR) modules_install

clean:
	make -C /lib/modules/$(HEADERSDIR)/build M=$(SOURCEDIR) clean
