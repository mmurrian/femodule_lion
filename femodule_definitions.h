// femodule_definitions.h
//
// Copyright (c) 2016-2021 The GRID Software Project. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#define FEMODULE_VENDOR_ID 0xfffa

#if defined(__LYNX)
#define FEMODULE_PRODUCT_ID 0x0002  // RadioLynx
#elif defined(__LION)
#define FEMODULE_PRODUCT_ID 0x0003  // RadioLion
#endif

#ifdef CONFIG_USB_DYNAMIC_MINORS
#define FEMODULE_MINOR_BASE 0
#else
#define FEMODULE_MINOR_BASE 96
#endif

// The single-bit quantization bitpacking for the Radiolynx has 2 samples
// per front-end byte, whereas lynx32 and lynx8 both have 1.
#ifdef __LYNX_SB
#define SAMPLES_PER_BYTE 2
#else
#define SAMPLES_PER_BYTE 1
#endif

// Configured FIFO buffer size on Cypress EZ-USB.  In other words,
// this is how often to expect a "buffer timestamp".
#define FEMODULE_BYTES_PER_BUFFER 1024

// Threshold above which a discovered gap in data will not be stuffed
#define FEMODULE_GAP_FILL_THRESHOLD (40 * FEMODULE_BYTES_PER_BUFFER)
// Limit in accumulated isochronous frame errors before streaming is aborted.
// Note that the frame error counter is incremented with each failed/empty
// frame and decremented with each successful frame.  Thus, frame success rates
// of 50% or better will never accumulate to failure.
//
// A value of 8,000 is nominally 1 second of continuous transfer failures.
#define FEMODULE_GNSS_ERRORS_LIMIT 8000

// Size of a single URB as well as the size of a single circular buffer element
#define FEMODULE_ISOC_IN_TRANSFER_SIZE 65536
// Number of URBs available to be queued asynchronously
#define FEMODULE_ISOC_IN_URB_COUNT 16
/* Number of elements in the circular buffer queue (each of *_TRANSFER_SIZE
 * size).  Note that each element referred to in an output queue is packet-
 * sized and not transfer-sized; so an output queue size should be some
 * multiple (packets per transfer) of the associated circular buffer size to
 * buffer the same amount of data.
 */
#define FEMODULE_GNSS_CIRCULAR_BUFFER_LENGTH 4096

#define FEMODULE_INT_IN_TRANSFER_SIZE 64
#define FEMODULE_INT_IN_URB_COUNT 4
#define FEMODULE_IMU_CIRCULAR_BUFFER_LENGTH 1024
#define FEMODULE_IMU_ERRORS_LIMIT 1000

// General USB vendor requests
#define VRQT_VENDOR_IN 0xC0
#define VRQT_VENDOR_OUT 0x40
#if defined(__LYNX)
// Send a transfer "stop" command
#define VRQ_STOP_XFER 0x00
// Send a transfer "start" command
#define VRQ_START_XFER 0x01
// Command for the RX/Maxim
#define VRQ_SET_REG 0x02
// Command for the PLL
#define VRQ_SET_PLL 0x08
#elif defined(__LION)
// Send a transfer "stop" command
#define VRQ_STOP_XFER 0x01
// Send a transfer "start" command
#define VRQ_START_XFER 0x02
// Command for the RX/Maxim
#define VRQ_SET_REG 0x04
#endif