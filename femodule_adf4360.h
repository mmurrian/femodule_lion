// femodule_adf4360.h
//
// Copyright (c) 2016-2021 The GRID Software Project. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#ifndef __LYNX
#error ADF4360 is only on the RadioLynx
#endif

static struct {
  const int CTRLBITS : 2;
  int COREPWLEVEL : 2;
  int CNTRRES : 1;
  int MUXOUTCTRL : 3;
  int PHASEDETPOL : 1;
  int CPTHREESTATE : 1;
  int CPGAIN : 1;
  int MUTETILLLD : 1;
  int OUTPWLEVEL : 2;
  int CURRSET1 : 3;
  int CURRSET2 : 3;
  int PWDN1 : 1;
  int PWDN2 : 1;
  int PRESCLRVAL : 2;
} adf4360_CONTROL_LATCH = {
    0b00,   // CTRLBITS
    0b00,   // COREPWLEVEL
    0b0,    // CNTRRES
    0b001,  // MUXOUTCTRL
    0b1,    // PHASEDETPOL
    0b0,    // CPTHREESTATE
    0b0,    // CPGAIN
    0b0,    // MUTETILLLD
    0b11,   // OUTPWLEVEL
    0b111,  // CURRSET1
    0b000,  // CURRSET2
    0b0,    // PWDN1
    0b0,    // PWDN2
    0b00    // PRESCLRVAL
};

static struct {
  const int CTRLBITS : 2;
  int REFERENCECNTR : 14;
  int ANTIBKLPULSEWIDTH : 2;
  int LOCKDETPREC : 1;
  int TESTMODEBIT : 1;
  int BANDSELCLK : 2;
  int reserved1 : 1;
  int reserved2 : 1;
} adf4360_R_COUNTER_LATCH = {0b01,              // CTRLBITS
                             0b00000000111100,  // REFERENCECNTR
                             0b00,              // ANTIBKLPULSEWIDTH
                             0b1,               // LOCKDETPREC
                             0b0,               // TESTMODEBIT
                             0b00,              // BANDSELCLK
                             0b0,
                             0b0};

static struct {
  const int CTRLBITS : 2;
  int ACNTR : 5;
  int reserved : 1;
  int BCNTR : 13;
  int CPGAIN : 1;
  int DIVBY2 : 1;
  int DIVBY2SEL : 1;
} adf4360_N_COUNTER_LATCH = {
    0b10,     // CTRLBITS
    0b00110,  // ACNTR
    0b0,
#ifdef __LYNX_EXT_CLK_10MHZ
    0b0001000001001,  // BCNTR
#else
    0b0000100001111,  // BCNTR
#endif
    0b0,  // CPGAIN
    0b1,  // DIVBY2
    0b0   // DIVBY2SEL
};

#define adf4360_config_size 3
static const char *adf4360_config[adf4360_config_size] = {
    (char *)&adf4360_CONTROL_LATCH, (char *)&adf4360_R_COUNTER_LATCH,
    (char *)&adf4360_N_COUNTER_LATCH};
