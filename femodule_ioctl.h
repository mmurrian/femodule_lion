// femodule_ioctl.h
//
// Copyright (c) 2016-2021 Matthew James Murrian. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#ifndef FEMODULE_IOCTL_H
#define FEMODULE_IOCTL_H
#include <linux/ioctl.h>
#include <linux/types.h>

// The ioctl interface allows arbitrary vendor requests to be submitted to the
// device. This allows the driver to do the narrow job that it is ideally suited
// for, facilitating efficient and reliable streaming of high-rate GNSS data,
// while allowing higher-level or auxiliary interactions between the device and
// userspace applications.

typedef struct {
  // Entries are direct analogs to arguments for the 'usb_control_msg' system
  // call.
  u8 request;   // USB message request value
  u16 value;    // USB message value
  u16 index;    // USB message index value
  void *data;   // Pointer to the data to send/receive
  u16 size;     // Length in bytes of the data to send/receiver
  int timeout;  // Time in milliseconds to wait for the message to complete
                // before timing out (if 0 the wait is forever)
} vendor_arg_t;

// A vendor request can be sent to the device by using on 'ioctl' system call on
// the open file descriptor of that device.
//
// Example code:
//
// int fd = open("/dev/radiolion", O_RDWR);
// vendor_arg_t args;
// args.request = VRQ_SET_REG;
// ioctl(fd, FEMODULE_IOCTL_VENDOR_OUT, &args);
//
#define FEMODULE_IOCTL_VENDOR_OUT _IOW('r', 1, vendor_arg_t *)
#define FEMODULE_IOCTL_VENDOR_IN _IOR('r', 2, vendor_arg_t *)

#endif