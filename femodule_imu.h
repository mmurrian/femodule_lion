// femodule_imu.h
//
// Copyright (c) 2016-2021 The GRID Software Project. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

// Used to facilitate generation of compile-time floating point constants
// and prevent having to use floating-point registers during run-time.
union ftoi {
  u32 ival;
  float fval;
};

// Note that the parameter values below ought to reflect the parameters in the
// firmware *.ihx file loaded onto the Cypress microcontroller, which programs
// the Bosch BMX055 IMU.  See rscore/lynx-firmware/README.md for a description
// of the various standard *.ihx files. The values below correspond to the
// default file lynx32_fx2_lp2wa.ihx. The IMU measurement rate was determined
// empirically.
static const union ftoi IMU_MEASUREMENT_RATE_HZ = {.fval = 146.47722269};
#ifdef __LYNX_SB
// The default single-bit IMU firmware, lynx32sb_fx2_llpw.ihx, has lower accel
// and gyro bandwidths than the two-bit firmware
static const union ftoi IMU_ANGULAR_RATE_BW_HZ = {.fval = 32.0};
static const union ftoi IMU_ACCELERATION_BW_HZ = {.fval = 31.25};
#else
// The default dual-bit IMU firmware is lynx32_fx2_lp2wa.ihx
static const union ftoi IMU_ANGULAR_RATE_BW_HZ = {.fval = 64.0};
static const union ftoi IMU_ACCELERATION_BW_HZ = {.fval = 62.5};
#endif
static const union ftoi LSB_TO_DEGK = {.fval = 0.5};
static const union ftoi TEMPERATURE_CENTER_DEGC = {.fval = 23.0};
#define IMU_RANGE_G 4
#define IMU_RANGE_DPS 250

#if IMU_RANGE_G == 2
#define LSB_TO_G (0.98 / 1000.0)
#endif
#if IMU_RANGE_G == 4
#define LSB_TO_G (1.95 / 1000.0)
#endif
#if IMU_RANGE_G == 8
#define LSB_TO_G (3.91 / 1000.0)
#endif

// BMX055 conversion to milliDeg/s:  3.8 for range 150 deg/s
//                                   7.6 for range 250 deg/s
//                                  15.3 for range 500 deg/s
//                                  30.5 for range 1000 deg/s
//
#if IMU_RANGE_DPS == 125
#define LSB_TO_DPS (3.8 / 1000.0)
#endif
#if IMU_RANGE_DPS == 250
#define LSB_TO_DPS (7.6 / 1000.0)
#endif
#if IMU_RANGE_DPS == 500
#define LSB_TO_DPS (15.3 / 1000.0)
#endif
#if IMU_RANGE_DPS == 1000
#define LSB_TO_DPS (30.5 / 1000.0)
#endif

#define PI 3.14159265358979
#define G_TO_MPS2 9.80665

static const union ftoi RANGE_MPS2 = {.fval = IMU_RANGE_G * G_TO_MPS2};
static const union ftoi LSB_TO_MPS2 = {.fval = LSB_TO_G * G_TO_MPS2};
static const union ftoi RANGE_RADPS = {.fval = IMU_RANGE_DPS * (PI / 180.0)};
static const union ftoi LSB_TO_RADPS = {.fval = LSB_TO_DPS * (PI / 180.0)};