// femodule.c
//
// Copyright (c) 2016-2021 The GRID Software Project. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#ifdef __LYNX_SB
#define __LYNX
#endif

#include "femodule.h"

#include "femodule_definitions.h"
#include "femodule_imu.h"
#include "femodule_ioctl.h"
#ifdef __LION
#include "femodule_max2771.h"
#else
#include "femodule_adf4360.h"
#include "femodule_max2769.h"
#endif

#if defined(__LYNX)
#if defined(__LYNX_EXT_CLK_10MHZ)
#define SAMPLE_FREQUENCY_NUMERATOR 10000000
#define SAMPLE_FREQUENCY_DENOMINATOR 1
#else
#define SAMPLE_FREQUENCY_NUMERATOR 19200000
#define SAMPLE_FREQUENCY_DENOMINATOR 2
#endif
#else
// TCXO_FREQUENCY specifies the actual frequency of the TCXO clock source (or,
// if applicable, an external clock source) presented to the XTAL pin on the
// MAX2771
#define TCXO_FREQUENCY_NUMERATOR 16368000
#define TCXO_FREQUENCY_DENOMINATOR 1

// RDIV values can be forced to specific values (if permissible). Otherwise,
// RDIV values will be automatically selected to produce IF values closest to
// their target values.

// Some RDIV values and their resulting PLL performances are provided below for
// the EVKIT-valued PLL charge pump filter. BW is the PLL bandwidth and PM is
// the Phase Margin.

// BW: 399 kHz PM: 23 deg
//#define FORCE_L1_RDIV 1
// BW: 260 kHz PM: 35 deg
#define FORCE_L1_RDIV 2

// BW: 476 kHz PM: 19 deg
//#define FORCE_L2_RDIV 1
// BW: 319 kHz PM: 29 deg
//#define FORCE_L2_RDIV 2
// BW: 249 kHz PM: 36 deg
#define FORCE_L2_RDIV 3

// BW: 488 kHz PM: 19 deg
//#define FORCE_L5_RDIV 1
// BW: 328 kHz PM: 29 deg
//#define FORCE_L5_RDIV 2
// BW: 256 kHz PM: 35 deg
#define FORCE_L5_RDIV 3

//#define TCXO_FREQUENCY_NUMERATOR 10000000
//#define TCXO_FREQUENCY_DENOMINATOR 1

// BW: 294 kHz PM: 32 deg
//#define FORCE_L1_RDIV 1

// BW: 362 kHz PM: 26 deg
//#define FORCE_L2_RDIV 1
// BW: 236 kHz PM: 38 deg
//#define FORCE_L2_RDIV 2
// BW: 8 kHz PM: 27 deg (high phase noise)
//#define FORCE_L2_RDIV 200

// BW: 372 kHz PM: 25 deg
//#define FORCE_L5_RDIV 1
// BW: 242 kHz PM: 37 deg
//#define FORCE_L5_RDIV 2
// BW: 19 kHz PM: 48 deg (high phase noise)
//#define FORCE_L5_RDIV 50

// REFERENCE_FREQUENCY specifies the desired reference frequency that will be
// produced at the CLKOUT pin of the primary L1 MAX2771 and provided to the
// CPLD.
#define REFERENCE_FREQUENCY_NUMERATOR 16368000
#define REFERENCE_FREQUENCY_DENOMINATOR 1
// The sample frequency returned back to the ADC_CLKIN pins of each
// MAX2771 is equal to the reference frequency multiplied by the specified ratio
// (as determined by CPLD programming).
#define SAMPLE_FREQUENCY_RATIO_NUMERATOR 1
#define SAMPLE_FREQUENCY_RATIO_DENOMINATOR 2
#define SAMPLE_FREQUENCY_L2_RATIO_NUMERATOR 1
#define SAMPLE_FREQUENCY_L2_RATIO_DENOMINATOR 2
#define SAMPLE_FREQUENCY_L5_RATIO_NUMERATOR 1
#define SAMPLE_FREQUENCY_L5_RATIO_DENOMINATOR 1

#define SAMPLE_FREQUENCY_NUMERATOR \
  (SAMPLE_FREQUENCY_RATIO_NUMERATOR * REFERENCE_FREQUENCY_NUMERATOR)
#define SAMPLE_FREQUENCY_DENOMINATOR \
  (SAMPLE_FREQUENCY_RATIO_DENOMINATOR * REFERENCE_FREQUENCY_DENOMINATOR)
#endif

static struct usb_device_id femodule_table[] = {
    {USB_DEVICE(FEMODULE_VENDOR_ID, FEMODULE_PRODUCT_ID)}, {}};
MODULE_DEVICE_TABLE(usb, femodule_table);

static int debug_level = DEBUG_LEVEL_DEBUG;
static int debug_trace = 0;
module_param(debug_level, int, S_IRUGO | S_IWUSR);
module_param(debug_trace, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(debug_level, "debug level (bitmask)");
MODULE_PARM_DESC(debug_trace, "enable function tracing");

/* Prevent races between open() and disconnect */
static struct mutex disconnect_mutex;

static struct usb_driver femodule_driver;

static struct usb_femodule_shared femodule_shared;

static char *stuffing_ptr;
static unsigned int stuffing_size;

static char *imuconfig_ptr;
static unsigned int imuconfig_size;

static inline void femodule_debug_data(const char *function, int size,
                                       const unsigned char *data) {
  int i;
  if ((debug_level & DEBUG_LEVEL_DEBUG) == DEBUG_LEVEL_DEBUG) {
    printk(KERN_DEBUG "[debug] %s: length = %d, data = ", function, size);
    for (i = 0; i < size; ++i) printk("%.2x ", data[i]);
    printk("\n");
  }
}

static int femodule_init_circular_buffer(struct cb_top *cb, int count,
                                         int size) {
#ifdef __DEBUG_PROTECT
  if (!cb) {
    DBG_DEBUG("cb is NULL");
    return -1;
  }
  if (count <= 0) {
    DBG_DEBUG("count is non-positive");
    return -1;
  }
  if (size <= 0) {
    DBG_DEBUG("count is non-positive");
    return -1;
  }
#endif
  // On the first call, cbuffer will be nullptr
  if (!cb->cbuffer) {
    cb->cbuffer = kzalloc(sizeof(struct cb_element) * size, GFP_KERNEL);
    if (!cb->cbuffer) return -1;
    cb->size = size;
    for (int i = 0; i < size; ++i) {
      cb->cbuffer[i].ptr = kmalloc(sizeof(char) * count, GFP_KERNEL);
      if (!cb->cbuffer[i].ptr) {
        cb->size = i;
        break;
      }
      cb->cbuffer[i].count = count;
    }
  }

  // In any case, reinitialize the buffer index
  atomic_set(&cb->idx, 0);

  return cb->size;
}

static int femodule_init_output_queue(struct oq_top *oq, int size) {
#ifdef __DEBUG_PROTECT
  if (!oq) {
    DBG_DEBUG("oq is NULL");
    return -1;
  }
  if (size <= 0) {
    DBG_DEBUG("size is non-positive");
    return -1;
  }
#endif

  // Allocate memory if not done already
  if (!oq->oqueue) {
    oq->oqueue = kzalloc(sizeof(struct cb_element) * size, GFP_KERNEL);
    if (!oq->oqueue) return -1;
    oq->size = size;
  }
  // In any case, reinitialize the buffer indices
  atomic_set(&oq->input_idx, 0);
  atomic_set(&oq->output_idx, 0);
  atomic_set(&oq->overrun, 0);
  oq->tIndex = 0;
  oq->output_oft = 0;

  return oq->size;
}

static void femodule_delete_circular_buffer(struct cb_top *cb) {
#ifdef __DEBUG_PROTECT
  if (!cb) {
    DBG_DEBUG("cb is NULL");
    return;
  }
#endif
  for (int i = 0; i < cb->size; ++i) {
    if (cb->cbuffer[i].ptr) {
      kfree(cb->cbuffer[i].ptr);
      cb->cbuffer[i].ptr = (char *)NULL;
      cb->cbuffer[i].count = 0;
    }
  }

  kfree(cb->cbuffer);
  cb->cbuffer = (struct cb_element *)NULL;
  cb->size = 0;
  atomic_set(&cb->idx, 0);
}

static void femodule_delete_output_queue(struct oq_top *oq) {
#ifdef __DEBUG_PROTECT
  if (!oq) {
    DBG_DEBUG("oq is NULL");
    return;
  }
#endif
  kfree(oq->oqueue);
  oq->oqueue = (struct cb_element *)NULL;
  oq->size = 0;
  atomic_set(&oq->input_idx, 0);
  atomic_set(&oq->output_idx, 0);
  atomic_set(&oq->overrun, 0);
  oq->tIndex = 0;
  oq->output_oft = 0;
}

static void femodule_delete_circular_buffers(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return;
  }
#endif
  if (dev->gnss_circular_buffer.cbuffer) {
    DBG_INFO("Freeing GNSS stream buffer");
    femodule_delete_circular_buffer(&dev->gnss_circular_buffer);
  }
  if (dev->gnss_output_queue.oqueue)
    femodule_delete_output_queue(&dev->gnss_output_queue);
  if (dev->imu_circular_buffer.cbuffer) {
    DBG_INFO("Freeing IMU stream buffer");
    femodule_delete_circular_buffer(&dev->imu_circular_buffer);
  }
  if (dev->imu_output_queue.oqueue)
    femodule_delete_output_queue(&dev->imu_output_queue);
}

static inline int bufferSizeForEndpoint(
    struct usb_endpoint_descriptor *const ep) {
#ifdef __DEBUG_PROTECT
  if (!ep) {
    DBG_DEBUG("ep is NULL");
    return -1;
  }
#endif
  const int wMaxPacketSize = le16_to_cpu(ep->wMaxPacketSize);
  const int mult = 1 + ((wMaxPacketSize >> 11) & 0x03);
  return mult * (wMaxPacketSize & 0x07ff);
}

static void femodule_init_circular_buffers(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return;
  }
#endif

  int retval;

  if (dev->isoc_in_endpoint) {
    DBG_INFO("Initializing GNSS stream buffer");
    const int gnss_buffer_size = bufferSizeForEndpoint(dev->isoc_in_endpoint);
    const int gnss_packets_per_transfer =
        FEMODULE_ISOC_IN_TRANSFER_SIZE / gnss_buffer_size;
    retval = femodule_init_circular_buffer(
        &dev->gnss_circular_buffer, FEMODULE_ISOC_IN_TRANSFER_SIZE,
        FEMODULE_GNSS_CIRCULAR_BUFFER_LENGTH);
    if (retval <= 0) {
      DBG_CRIT("Failed to allocate GNSS circular buffer");
      return;
    }

    retval = femodule_init_output_queue(
        &dev->gnss_output_queue,
        dev->gnss_circular_buffer.size * gnss_packets_per_transfer);
    if (retval <= 0) {
      DBG_CRIT("Failed to allocate GNSS output queue");
      goto fail_return;
    }
  }
  if (dev->int_in_endpoint) {
    DBG_INFO("Initializing IMU stream buffer");
    const int imu_buffer_size = bufferSizeForEndpoint(dev->int_in_endpoint);
    const int imu_packets_per_transfer =
        FEMODULE_INT_IN_TRANSFER_SIZE / imu_buffer_size;
    retval = femodule_init_circular_buffer(&dev->imu_circular_buffer,
                                           FEMODULE_INT_IN_TRANSFER_SIZE,
                                           FEMODULE_IMU_CIRCULAR_BUFFER_LENGTH);
    if (retval <= 0) {
      DBG_CRIT("Failed to allocate IMU circular buffer");
      goto fail_return;
    }

    retval = femodule_init_output_queue(
        &dev->imu_output_queue,
        dev->imu_circular_buffer.size * imu_packets_per_transfer);
    if (retval <= 0) {
      DBG_CRIT("Failed to allocate IMU output queue");
      goto fail_return;
    }
  }
  return;

fail_return:
  femodule_delete_circular_buffers(dev);
}

static inline bool oq_overrun(struct oq_top *oq) {
  const int input_idx = atomic_read(&oq->input_idx);
  const int output_idx = atomic_read(&oq->output_idx);
  const int delta_idx = input_idx - output_idx;

  if (delta_idx <= (int)oq->size) {
    if (atomic_cmpxchg(&oq->overrun, 1, 0))
      DBG_ERR("Output queue overrun cleared.");
    return 0;
  } else {
    if (!atomic_cmpxchg(&oq->overrun, 0, 1)) DBG_ERR("Output queue overrun!");
    return 1;
  }
}

static inline void femodule_commit_to_output_queue(struct oq_top *oq, char *ptr,
                                                   size_t count) {
#ifdef __DEBUG_PROTECT
  if (!ptr) {
    DBG_DEBUG("ptr is NULL");
    return;
  }
  if (!oq) {
    DBG_DEBUG("oq is NULL");
    return;
  }
  if (!oq->oqueue) {
    DBG_DEBUG("oqueue is NULL");
    return;
  }
#endif
  if (!count) return;

  const int input_idx = atomic_read(&oq->input_idx);
  oq->oqueue[input_idx % oq->size].ptr = ptr;
  oq->oqueue[input_idx % oq->size].count = count;
  oq->tIndex += count * SAMPLES_PER_BYTE;
  // Add 1 to input_idx only if it is still the value read above. This
  // is unnecessary caution since this queue has a single producer but
  // atomic_cmpxchg also implies a general memory barrier when successful.
  if (atomic_cmpxchg(&oq->input_idx, input_idx, input_idx + 1) != input_idx)
    DBG_DEBUG("input_idx apparently modified by other producer.");
  oq_overrun(oq);
  // Wake up a potentially blocked reader
  wake_up_interruptible(&oq->wqueue);
}

static inline struct cb_element *femodule_cb_next_element(struct cb_top *cb) {
#ifdef __DEBUG_PROTECT
  if (!cb) {
    DBG_DEBUG("cb is NULL");
    return (struct cb_element *)NULL;
  }
#endif
  const int idx = (atomic_inc_return(&cb->idx) - 1) % cb->size;
  struct cb_element *const retptr = &cb->cbuffer[idx];
  // Memory barrier to ensure cb->idx is incremented after access.
  mb();
  return retptr;
}

struct timestamp_fields {
  unsigned int frame;
  unsigned int uframe;
  unsigned char seq;
};

static inline int femodule_calculate_delta(
    const unsigned char *const timestamp0,
    const unsigned char *const timestamp1) {
#ifdef __DEBUG_PROTECT
  if (unlikely(!timestamp0)) {
    DBG_DEBUG("timestamp0 is NULL");
    return -1;
  }
  if (unlikely(!timestamp1)) {
    DBG_DEBUG("timestamp1 is NULL");
    return -1;
  }
#endif

  const int ts0 = (timestamp0[0] & 0xff) | (timestamp0[1] << 8);
  const int ts1 = (timestamp1[0] & 0xff) | (timestamp1[1] << 8);

  if (likely(ts1 >= ts0)) {
    return (ts1 - ts0) * FEMODULE_BYTES_PER_BUFFER;
  } else {
    return (ts1 + 0x10000 - ts0) * FEMODULE_BYTES_PER_BUFFER;
  }
}

static inline int femodule_calculate_gap(
    const unsigned char *const timestamp0,
    const unsigned char *const timestamp1) {
#ifdef __DEBUG_PROTECT
  if (unlikely(!timestamp0)) {
    DBG_DEBUG("timestamp0 is NULL");
    return -1;
  }
  if (unlikely(!timestamp1)) {
    DBG_DEBUG("timestamp1 is NULL");
    return -1;
  }
#endif

  return femodule_calculate_delta(timestamp0, timestamp1) -
         FEMODULE_BYTES_PER_BUFFER;
}

static inline void femodule_fill_gap(struct oq_top *const oq,
                                     const unsigned char *const ts0,
                                     const unsigned char *const ts1) {
  int gap_bytes = femodule_calculate_gap(ts0, ts1);
  if (unlikely(gap_bytes > FEMODULE_GAP_FILL_THRESHOLD)) {
#ifdef __DEBUG_DEVICE
    DBG_DEBUG("Refusing to fill %d byte gap (%d byte threshold)", gap_bytes,
              FEMODULE_GAP_FILL_THRESHOLD);
#endif
    gap_bytes = 0;
  }
#ifdef __DEBUG_DEVICE
  if (gap_bytes) DBG_DEBUG("Random-filled %d byte gap", gap_bytes);
#endif
  while (unlikely(gap_bytes > 0)) {
    const int gap_commit_bytes =
        (gap_bytes > stuffing_size) ? stuffing_size : gap_bytes;
    femodule_commit_to_output_queue(oq, stuffing_ptr, gap_commit_bytes);
    gap_bytes -= gap_commit_bytes;
  }
}

static void femodule_stop_imu_reader(void) {
  if (femodule_shared.dev_imu.int_in_endpoint) {
    atomic_set(&femodule_shared.dev_imu.running, 0);
    if (waitqueue_active(&femodule_shared.dev_imu.imu_output_queue.wqueue))
      wake_up_interruptible(&femodule_shared.dev_imu.imu_output_queue.wqueue);
  }
}

static void femodule_stop_gnss_reader(void) {
  if (femodule_shared.dev_gnss.isoc_in_endpoint) {
    atomic_set(&femodule_shared.dev_gnss.running, 0);
    if (waitqueue_active(&femodule_shared.dev_gnss.gnss_output_queue.wqueue))
      wake_up_interruptible(&femodule_shared.dev_gnss.gnss_output_queue.wqueue);
  }
}

static void femodule_stop_readers(void) {
  femodule_stop_gnss_reader();
  femodule_stop_imu_reader();
}

static void femodule_isoc_in_work(struct work_struct *work_arg) {
#ifdef __DEBUG_PROTECT
  if (unlikely(!work_arg)) {
    DBG_DEBUG("work_arg is NULL");
    return;
  }
#endif
  struct femodule_transfer *const transfer =
      container_of(work_arg, struct femodule_transfer, work);
  struct usb_femodule *const dev = transfer->dev;
  struct urb *const urb_ptr = transfer->urb;
  bool trigger_abort = false;
  bool excess_frame_errors = false;
  int retval;

  if (unlikely(urb_ptr->status)) {
    if (urb_ptr->status == -ENOENT || urb_ptr->status == -ECONNRESET ||
        urb_ptr->status == -ESHUTDOWN) {
      // Abort for fatal URB errors
      trigger_abort = true;
      goto abort;
    } else {
#ifdef __DEBUG_DEVICE
      DBG_DEBUG("Non-zero GNSS USB transfer status (%d)", urb_ptr->status);
#endif
      // Otherwise, count a non-zero URB status as a frame error.
      do {
        retval = mutex_lock_interruptible(&dev->mutex_isoc_in_data);
      } while (retval == -EINTR);
      trigger_abort =
          (++dev->isoc_in_frame_errors == FEMODULE_GNSS_ERRORS_LIMIT);
      excess_frame_errors =
          (dev->isoc_in_frame_errors >= FEMODULE_GNSS_ERRORS_LIMIT);
      mutex_unlock(&dev->mutex_isoc_in_data);
      // Resubmit
      goto resubmit;
    }
  }

  // Hold mutex throughout duration of work task to enforce sequencing with
  // other work tasks. Work tasks cannot be allowed to interlace.
  do {
    retval = mutex_lock_interruptible(&dev->mutex_isoc_in_data);
  } while (retval == -EINTR);
  const bool excess_frame_errors_prior =
      dev->isoc_in_frame_errors >= FEMODULE_GNSS_ERRORS_LIMIT ||
      dev->isoc_in_frame_empties >= FEMODULE_GNSS_ERRORS_LIMIT;
  for (int i = 0; i < urb_ptr->number_of_packets; ++i) {
    // Any non-zero frame status is considered a frame error.
    if (unlikely(urb_ptr->iso_frame_desc[i].status)) {
#ifdef __DEBUG_DEVICE
      switch (urb_ptr->iso_frame_desc[i].status) {
        case -ENOSR:
          DBG_DEBUG("USB frame overrun on GNSS stream!");
          break;
        case -EPROTO:
          DBG_DEBUG("USB protocol error. Check your device connection.");
          break;
        default:
          DBG_DEBUG("Non-zero GNSS USB frame status (%d)",
                    urb_ptr->iso_frame_desc[i].status);
          break;
      }
#endif
      ++dev->isoc_in_frame_errors;
    } else if (unlikely(urb_ptr->iso_frame_desc[i].actual_length == 0)) {
      ++dev->isoc_in_frame_empties;
    } else {
      dev->isoc_in_frame_empties = 0;
      // Successful frames decrement from the frame error count. Thus, the
      // threshold can only be exceeded if the majority of frames fail.
      if (unlikely(dev->isoc_in_frame_errors)) --dev->isoc_in_frame_errors;

      unsigned char *commit_ptr =
          urb_ptr->transfer_buffer + urb_ptr->iso_frame_desc[i].offset;
      unsigned int pending_bytes = urb_ptr->iso_frame_desc[i].actual_length;

      while (pending_bytes > 0) {
        const unsigned int commit_bytes =
            (pending_bytes > FEMODULE_BYTES_PER_BUFFER)
                ? FEMODULE_BYTES_PER_BUFFER
                : pending_bytes;
        if (unlikely(dev->isoc_in_buffer_discard > 0)) {
          --dev->isoc_in_buffer_discard;
        } else {
          if (likely(dev->isoc_in_timestamp_valid))
            femodule_fill_gap(&dev->gnss_output_queue, dev->isoc_in_timestamp,
                              commit_ptr);
          // The tIndex value associated with the current timestamp is that
          // value not already advanced by its containing buffer.
          dev->isoc_in_tindex = dev->gnss_output_queue.tIndex;
          // Commit buffer to output queue
          femodule_commit_to_output_queue(&dev->gnss_output_queue, commit_ptr,
                                          commit_bytes);
        }
        // Store timestamp of current buffer
        dev->isoc_in_timestamp[0] = commit_ptr[0];
        dev->isoc_in_timestamp[1] = commit_ptr[1];
        dev->isoc_in_timestamp_valid = true;

        commit_ptr += commit_bytes;
        pending_bytes -= commit_bytes;
      }
    }
  }
  excess_frame_errors =
      dev->isoc_in_frame_errors >= FEMODULE_GNSS_ERRORS_LIMIT ||
      dev->isoc_in_frame_empties >= FEMODULE_GNSS_ERRORS_LIMIT;

  trigger_abort = !excess_frame_errors_prior && excess_frame_errors;
  mutex_unlock(&dev->mutex_isoc_in_data);

resubmit:
  /* Resubmit if we're still running. */
  if (likely(!excess_frame_errors && atomic_read(&dev->running) && dev->udev)) {
    struct cb_element *const cb_elmnt =
        femodule_cb_next_element(&dev->gnss_circular_buffer);
    urb_ptr->transfer_buffer = cb_elmnt->ptr;
    urb_ptr->transfer_buffer_length = cb_elmnt->count;
    if (unlikely(usb_submit_urb(urb_ptr, GFP_KERNEL))) {
      DBG_ERR("Failed to resubmit URB");
      trigger_abort = true;
    }
  }

abort:
  /* Abort and notify if one has been triggered. */
  if (unlikely(trigger_abort && atomic_read(&dev->running)))
    femodule_stop_readers();
}

static void femodule_isoc_in_callback(struct urb *urb_ptr) {
#ifdef __DEBUG_PROTECT
  if (!urb_ptr) {
    DBG_DEBUG("urb_ptr is NULL");
    return;
  }
#endif
  struct femodule_transfer *const transfer = urb_ptr->context;
  struct workqueue_struct *const workqueue = transfer->dev->workqueue;
  // All URB handling takes place in the work queue. If work fails to be
  // queued because a prior task has yet to complete then streaming will
  // be aborted.
  if (unlikely((urb_ptr->status != -ENOENT) &&
               atomic_read(&transfer->dev->running) &&
               !queue_work(workqueue, &transfer->work)))
    femodule_stop_readers();
}

#define SPECIALPACKET_SYNC (0xFF)
#define SPECIALPACKET_ERROR (0xEE)
#define SPECIALPACKET_IMU (0x83)
#define SPECIALPACKET_TRG (0x38)

// Variant used for GBX4
static u16 femodule_fletcher16(const u8 *data, u32 bytes) {
  u16 sum1 = 0xff, sum2 = 0xff;
  u32 tlen;

  while (bytes) {
    tlen = ((bytes >= 20) ? 20 : bytes);
    bytes -= tlen;
    do {
      sum2 += sum1 += *data++;
      tlen--;
    } while (tlen);
    sum1 = (sum1 & 0xff) + (sum1 >> 8);
    sum2 = (sum2 & 0xff) + (sum2 >> 8);
  }
  /* Second reduction step to reduce sums to 8 bits */
  sum1 = (sum1 & 0xff) + (sum1 >> 8);
  sum2 = (sum2 & 0xff) + (sum2 >> 8);
  return (sum2 << 8) | sum1;
}

// Variant used for GBX3
/*
static u16 femodule_fletcher16(const u8* buffer, const u32 numBytes) {
  u8 checkA = 0;
  u8 checkB = 0;
  u32 i;
  for (i=0; i < numBytes; ++i) {
   checkA += buffer[i];
   checkB += checkA;
  }
  return (((u16)checkA) << 8) | ((u16)checkB);
}*/

struct femodule_triggerpacket_struct {
  u8 msg_header;
  u8 seq;
  u8 timestamp[2];
} __attribute__((__packed__));

struct femodule_errorpacket_struct {
  u8 msg_header;
  u8 error_code;
} __attribute__((__packed__));

struct femodule_imupacket_struct {
  u8 msg_header;
  u8 timestamp_acc[2];
  u16 acceleration[3];
  s8 acc_temp;
  u8 timestamp_gyr[2];
  u16 angularRate[3];
} __attribute__((__packed__));

struct femodule_reportimu_struct {
  u8 sync_bytes[2];
  u8 report_type;
  u8 stream_id;
  u32 report_size;

  u32 tIndexTrunc;
  u16 acceleration[3];
  u16 angularRate[3];
  s8 temperature;

  u16 checksum;
} __attribute__((__packed__));

struct femodule_reportimuconfig_struct {
  u8 sync_bytes[2];
  u8 report_type;
  u8 stream_id;
  u32 report_size;

  u64 tIndexk;

  // The following fields are declared as u32 despite containing what are
  // actually floating-point values. This is used in conjunction with a
  // `union ftoi' found in femodule_max2769.h to enable compile-time
  // declaration of floating-point constants without requiring the use of
  // floating-point registers at run-time (strictly enforced for AArch64 and
  // generally a good idea for any platforms kernel module).
  u32 rangeMetersPerSecSq;
  u32 rangeRadPerSec;
  u32 lsbToMetersPerSecSq;
  u32 lsbToRadPerSec;
  u32 accelerationBandwidthHz;
  u32 angularRateBandwidthHz;
  u32 measurementRateHz;

  s64 sampleFreqNumerator;
  s32 sampleFreqDenominator;
  // These fields similarly contain what are actually floating-point values.
  u32 lsbToDegK;
  u32 temperatureCenterDegC;

  u16 checksum;
} __attribute__((__packed__));

struct femodule_reporttriggertime_struct {
  u8 sync_bytes[2];
  u8 report_type;
  u8 stream_id;
  u32 report_size;

  u64 tIndexk;
  u8 seq;

  u16 checksum;
} __attribute__((__packed__));

#define ReportTypeIMU (0x06)
#define ReportIMUPayloadSize (17)
#define ReportTypeIMUConfig (0x07)
#define ReportIMUConfigPayloadSize (56)
#define ReportTypeTriggerTime (0x10)
#define ReportTriggerTimePayloadSize (9)
#define FIRST_SYNC_BYTE (0x55)
#define SECOND_SYNC_BYTE (0x74)

static void femodule_imuconfig_fill_initial(void) {
  struct femodule_reportimuconfig_struct *const report_ptr =
      (struct femodule_reportimuconfig_struct *)imuconfig_ptr;
  report_ptr->sync_bytes[0] = FIRST_SYNC_BYTE;
  report_ptr->sync_bytes[1] = SECOND_SYNC_BYTE;
  report_ptr->report_type = ReportTypeIMUConfig;
  report_ptr->stream_id = 0x00;
  report_ptr->report_size = cpu_to_le32(ReportIMUConfigPayloadSize);

  // report_ptr->tIndexk = 0;
  report_ptr->rangeMetersPerSecSq = RANGE_MPS2.ival;
  cpu_to_le32s(&report_ptr->rangeMetersPerSecSq);
  report_ptr->rangeRadPerSec = RANGE_RADPS.ival;
  cpu_to_le32s(&report_ptr->rangeRadPerSec);
  report_ptr->lsbToMetersPerSecSq = LSB_TO_MPS2.ival;
  cpu_to_le32s(&report_ptr->lsbToMetersPerSecSq);
  report_ptr->lsbToRadPerSec = LSB_TO_RADPS.ival;
  cpu_to_le32s(&report_ptr->lsbToRadPerSec);
  report_ptr->lsbToDegK = LSB_TO_DEGK.ival;
  cpu_to_le32s(&report_ptr->lsbToDegK);
  report_ptr->temperatureCenterDegC = TEMPERATURE_CENTER_DEGC.ival;
  cpu_to_le32s(&report_ptr->temperatureCenterDegC);
  report_ptr->accelerationBandwidthHz = IMU_ACCELERATION_BW_HZ.ival;
  cpu_to_le32s(&report_ptr->accelerationBandwidthHz);
  report_ptr->angularRateBandwidthHz = IMU_ANGULAR_RATE_BW_HZ.ival;
  cpu_to_le32s(&report_ptr->angularRateBandwidthHz);
  report_ptr->measurementRateHz = IMU_MEASUREMENT_RATE_HZ.ival;
  cpu_to_le32s(&report_ptr->measurementRateHz);
  report_ptr->sampleFreqNumerator = cpu_to_le64(SAMPLE_FREQUENCY_NUMERATOR);
  report_ptr->sampleFreqDenominator = cpu_to_le32(SAMPLE_FREQUENCY_DENOMINATOR);
  // report_ptr->checksum = 0;
}

static void femodule_imuconfig_fill_commit(struct usb_femodule *const dev) {
  struct femodule_reportimuconfig_struct *const report_ptr =
      (struct femodule_reportimuconfig_struct *)imuconfig_ptr;
  const u32 reportsize = sizeof(struct femodule_reportimuconfig_struct);
  int retval;
  do {
    retval =
        mutex_lock_interruptible(&femodule_shared.dev_gnss.mutex_isoc_in_data);
  } while (retval == -EINTR);
  const u64 tIndexk = femodule_shared.dev_gnss.gnss_output_queue.tIndex;
  mutex_unlock(&femodule_shared.dev_gnss.mutex_isoc_in_data);
  const u32 tIndexTrunc = (u32)(0xFFFFFFFF & tIndexk);
  dev->int_in_last_config = tIndexTrunc;
  report_ptr->tIndexk = cpu_to_le64(tIndexk);
  report_ptr->checksum =
      cpu_to_le16(femodule_fletcher16(imuconfig_ptr, reportsize - sizeof(u16)));
}

static u64 femodule_tindex_from_timestamp(const unsigned char *timestamp_meas) {
  int retval;
  do {
    retval =
        mutex_lock_interruptible(&femodule_shared.dev_gnss.mutex_isoc_in_data);
  } while (retval == -EINTR);

  // Calculate the byte delta between the timestamps in either direction. Assume
  // the smallest calculated delta between the two is the correct one.
  const int forward_delta = femodule_calculate_delta(
      femodule_shared.dev_gnss.isoc_in_timestamp, timestamp_meas);
  const int reverse_delta = femodule_calculate_delta(
      timestamp_meas, femodule_shared.dev_gnss.isoc_in_timestamp);
  const u64 tIndexBase = femodule_shared.dev_gnss.isoc_in_tindex;

  mutex_unlock(&femodule_shared.dev_gnss.mutex_isoc_in_data);

  if (likely(abs(forward_delta) < abs(reverse_delta)))
    return tIndexBase + forward_delta;
  else
    return tIndexBase - reverse_delta;
}

static void femodule_convert_to_gbx_reports_in_place(
    unsigned char *const sp_ptr, u32 *count) {
  if (!count)
    return;
  else if (!sp_ptr)
    goto exit_empty;

  u8 gbx_ptr[64];
  u32 sp_pos = 0;
  u32 gbx_pos = 0;

  while (sp_pos < *count) {
    const u8 *const sp_ptr_u8 = &sp_ptr[sp_pos];
    u8 *const gbx_ptr_u8 = &gbx_ptr[gbx_pos];

    if (*sp_ptr_u8 == SPECIALPACKET_IMU &&
        *count >= sizeof(struct femodule_imupacket_struct) + sp_pos) {
      const struct femodule_imupacket_struct *const packet_ptr =
          (struct femodule_imupacket_struct *)sp_ptr_u8;
      struct femodule_reportimu_struct *const report_ptr =
          (struct femodule_reportimu_struct *)gbx_ptr_u8;

      const u32 reportsize = sizeof(struct femodule_reportimu_struct);

      const u64 tIndexAcc =
          femodule_tindex_from_timestamp(packet_ptr->timestamp_acc);
      const u64 tIndexGyr =
          femodule_tindex_from_timestamp(packet_ptr->timestamp_gyr);
      const u32 tIndexTrunc =
          (u32)(0xFFFFFFFF & ((tIndexGyr - tIndexAcc) / 2 + tIndexAcc));

      // GBX Report header section
      report_ptr->sync_bytes[0] = FIRST_SYNC_BYTE;
      report_ptr->sync_bytes[1] = SECOND_SYNC_BYTE;
      report_ptr->report_type = ReportTypeIMU;
      report_ptr->stream_id = 0x00;
      report_ptr->report_size = cpu_to_le32(ReportIMUPayloadSize);

      // GBX Report payload section
      report_ptr->tIndexTrunc = cpu_to_le32(tIndexTrunc);
      for (int ii = 0; ii < 3; ++ii) {
        report_ptr->acceleration[ii] = packet_ptr->acceleration[ii];
        report_ptr->angularRate[ii] = packet_ptr->angularRate[ii];
      }
      report_ptr->temperature = packet_ptr->acc_temp;

      // GBX Report footer section
      report_ptr->checksum =
          cpu_to_le16(femodule_fletcher16(gbx_ptr, reportsize - sizeof(u16)));

      // Advance positions within special packet buffer and GBX output buffer
      sp_pos += sizeof(struct femodule_imupacket_struct);
      gbx_pos += reportsize;
    } else if (*sp_ptr_u8 == SPECIALPACKET_TRG &&
               *count >=
                   sizeof(struct femodule_triggerpacket_struct) + sp_pos) {
      const struct femodule_triggerpacket_struct *const packet_ptr =
          (struct femodule_triggerpacket_struct *)sp_ptr_u8;
      struct femodule_reporttriggertime_struct *const report_ptr =
          (struct femodule_reporttriggertime_struct *)gbx_ptr_u8;

      const u32 reportsize = sizeof(struct femodule_reporttriggertime_struct);

      // GBX Report header section
      report_ptr->sync_bytes[0] = FIRST_SYNC_BYTE;
      report_ptr->sync_bytes[1] = SECOND_SYNC_BYTE;
      report_ptr->report_type = ReportTypeTriggerTime;
      report_ptr->stream_id = 0x00;
      report_ptr->report_size = cpu_to_le32(ReportTriggerTimePayloadSize);

      // GBX Report payload section
      report_ptr->tIndexk =
          cpu_to_le64(femodule_tindex_from_timestamp(packet_ptr->timestamp));
      report_ptr->seq = packet_ptr->seq;

      // GBX Report footer section
      report_ptr->checksum =
          cpu_to_le16(femodule_fletcher16(gbx_ptr, reportsize - sizeof(u16)));

      // Advance positions within special packet buffer and GBX output buffer
      sp_pos += sizeof(struct femodule_triggerpacket_struct);
      gbx_pos += reportsize;
    } else if (*sp_ptr_u8 == SPECIALPACKET_ERROR &&
               *count >= sizeof(struct femodule_errorpacket_struct) + sp_pos) {
      // Advance position within special packet buffer
      sp_pos += sizeof(struct femodule_errorpacket_struct);
    } else {
      break;
    }
  }

  if (gbx_pos > 0) {
    memcpy(sp_ptr, gbx_ptr, gbx_pos);
    *count = gbx_pos;
  }

  return;
exit_empty:
  *count = 0;
}

static void femodule_int_in_work(struct work_struct *work_arg) {
#ifdef __DEBUG_PROTECT
  if (!work_arg) {
    DBG_DEBUG("work_arg is NULL");
    return;
  }
#endif

  struct femodule_transfer *const transfer =
      container_of(work_arg, struct femodule_transfer, work);
  struct usb_femodule *const dev = transfer->dev;
  struct urb *const urb_ptr = transfer->urb;
  u32 report_length = urb_ptr->actual_length;

  bool trigger_abort = false;
  bool excess_frame_errors = false;
  int retval;

  if (urb_ptr->status) {
    if (urb_ptr->status == -ENOENT || urb_ptr->status == -ECONNRESET ||
        urb_ptr->status == -ESHUTDOWN) {
      // Abort for fatal URB errors
      trigger_abort = true;
      goto abort;
    } else {
#ifdef __DEBUG_DEVICE
      switch (urb_ptr->status) {
        case -EPROTO:
          DBG_DEBUG("USB protocol error. Check your device connection.");
          break;
        default:
          DBG_DEBUG("Non-zero GNSS USB frame status (%d)", urb_ptr->status);
          break;
      }
#endif
      // Otherwise, count a non-zero URB status as a frame error.
      do {
        retval = mutex_lock_interruptible(&dev->mutex_int_in_data);
      } while (retval == -EINTR);
      trigger_abort = (++dev->int_in_frame_errors == FEMODULE_IMU_ERRORS_LIMIT);
      mb();
      excess_frame_errors =
          (dev->int_in_frame_errors >= FEMODULE_IMU_ERRORS_LIMIT);
      mutex_unlock(&dev->mutex_int_in_data);
      // Resubmit
      goto resubmit;
    }
  }

  // Hold mutex throughout duration of work task to enforce sequencing with
  // other work tasks. Work tasks cannot be allowed to interlace.
  do {
    retval = mutex_lock_interruptible(&dev->mutex_int_in_data);
  } while (retval == -EINTR);
  femodule_convert_to_gbx_reports_in_place(urb_ptr->transfer_buffer,
                                           &report_length);
  if (report_length) {
    // Successful frames decrement from the frame error count. Thus, the
    // threshold can only be exceeded if the majority of frames fail.
    if (unlikely(dev->int_in_frame_errors)) --dev->int_in_frame_errors;
    // Insert an IMU config report into the output queue if its time.
    const struct femodule_reportimu_struct *const report_ptr =
        (struct femodule_reportimu_struct *)urb_ptr->transfer_buffer;
    // If the IMU measurement report isn't first on the queue then it
    // isn't in this queue at all. Wait until the next special packet.
    if (report_ptr->report_type == ReportTypeIMU) {
      const s32 delta_last_config =
          report_ptr->tIndexTrunc - dev->int_in_last_config;
      // The effective periodicity depends on byte-rate of the device. However,
      // it needs to be at least as frequent as a single roll-over of the
      // u32-type.
      const s32 config_periodicity = 1 << (sizeof(u32) * 8 - 3);
      if (delta_last_config >= config_periodicity) {
        femodule_imuconfig_fill_commit(dev);
        femodule_commit_to_output_queue(
            &dev->imu_output_queue, imuconfig_ptr,
            sizeof(struct femodule_reportimuconfig_struct));
      }
    }

    femodule_commit_to_output_queue(&dev->imu_output_queue,
                                    urb_ptr->transfer_buffer, report_length);
  }
  mutex_unlock(&dev->mutex_int_in_data);

resubmit:
  /* Resubmit if we're still running. */
  if (!excess_frame_errors && atomic_read(&dev->running) && dev->udev) {
    struct cb_element *const cb_elmnt =
        femodule_cb_next_element(&dev->imu_circular_buffer);
    urb_ptr->transfer_buffer = cb_elmnt->ptr;
    urb_ptr->transfer_buffer_length = cb_elmnt->count;
    if (usb_submit_urb(urb_ptr, GFP_KERNEL)) {
      DBG_ERR("Failed to resubmit URB");
      trigger_abort = true;
    }
  }

abort:
  /* Abort and notify if one has been triggered. */
  if (trigger_abort && atomic_read(&dev->running)) femodule_stop_readers();
}

static void femodule_int_in_callback(struct urb *urb_ptr) {
#ifdef __DEBUG_PROTECT
  if (!urb_ptr) {
    DBG_DEBUG("urb_ptr is NULL");
    return;
  }
#endif
  struct femodule_transfer *const transfer = urb_ptr->context;
  struct workqueue_struct *const workqueue = transfer->dev->workqueue;
  // All URB handling takes place in the work queue. If work fails to be
  // queued because a prior task has yet to complete then streaming will
  // be aborted.
  if ((urb_ptr->status != -ENOENT) && atomic_read(&transfer->dev->running) &&
      !queue_work(workqueue, &transfer->work))
    femodule_stop_readers();
}

static struct urb *femodule_init_isoc_urb(struct femodule_transfer *transfer,
                                          int packetsPerTransfer) {
#ifdef __DEBUG_PROTECT
  if (!transfer) {
    DBG_DEBUG("transfer is NULL");
    return (struct urb *)NULL;
  }
  if (!transfer->dev) {
    DBG_DEBUG("dev is NULL");
    return (struct urb *)NULL;
  }
  if (!transfer->dev->udev) {
    DBG_DEBUG("udev is NULL");
    return (struct urb *)NULL;
  }
  if (!transfer->dev->isoc_in_endpoint) {
    DBG_DEBUG("isoc_in_endpoint is NULL");
    return (struct urb *)NULL;
  }
  if (packetsPerTransfer <= 0) {
    DBG_DEBUG("packetsPerTransfer is non-positive");
    return (struct urb *)NULL;
  }
#endif

  struct usb_femodule *const dev = transfer->dev;
  const int packetSize = bufferSizeForEndpoint(dev->isoc_in_endpoint);
  struct urb *const urb_ptr = usb_alloc_urb(packetsPerTransfer, GFP_KERNEL);
  if (!urb_ptr) {
    DBG_CRIT("Failed to allocated USB URB");
    return (struct urb *)NULL;
  }
  struct cb_element *const cb_elmnt =
      femodule_cb_next_element(&dev->gnss_circular_buffer);
#ifdef __DEBUG_PROTECT
  if (!cb_elmnt) {
    DBG_DEBUG("cb_elmnt is NULL");
    return (struct urb *)NULL;
  }
#endif

  urb_ptr->dev = dev->udev;
  urb_ptr->context = transfer;
  urb_ptr->pipe =
      usb_rcvisocpipe(dev->udev, dev->isoc_in_endpoint->bEndpointAddress);
  urb_ptr->interval = dev->isoc_in_endpoint->bInterval;
  urb_ptr->complete = femodule_isoc_in_callback;
  urb_ptr->transfer_buffer = cb_elmnt->ptr;
  urb_ptr->transfer_buffer_length = cb_elmnt->count;
  urb_ptr->transfer_flags = URB_ISO_ASAP;
  urb_ptr->number_of_packets = packetsPerTransfer;

  for (int j = 0; j < packetsPerTransfer; ++j) {
    urb_ptr->iso_frame_desc[j].offset = j * packetSize;
    urb_ptr->iso_frame_desc[j].length = packetSize;
  }

  return urb_ptr;
}

static struct urb *femodule_init_int_urb(struct femodule_transfer *transfer) {
#ifdef __DEBUG_PROTECT
  if (!transfer) {
    DBG_ERR("transfer is NULL");
    return (struct urb *)NULL;
  }
  if (!transfer->dev) {
    DBG_CRIT("dev is NULL");
    return (struct urb *)NULL;
  }
  if (!transfer->dev->udev) {
    DBG_ERR("udev is NULL");
    return (struct urb *)NULL;
  }
#endif

  struct usb_femodule *const dev = transfer->dev;
  struct urb *const urb_ptr = usb_alloc_urb(0, GFP_KERNEL);
  if (!urb_ptr) {
    DBG_CRIT("Failed to allocate USB URB");
    return (struct urb *)NULL;
  }
  struct cb_element *const cb_elmnt =
      femodule_cb_next_element(&dev->imu_circular_buffer);
#ifdef __DEBUG_PROTECT
  if (!cb_elmnt) {
    DBG_DEBUG("cb_elmnt is NULL");
    return (struct urb *)NULL;
  }
#endif

  usb_fill_int_urb(
      urb_ptr, dev->udev,
      usb_rcvintpipe(dev->udev, dev->int_in_endpoint->bEndpointAddress),
      cb_elmnt->ptr, cb_elmnt->count, femodule_int_in_callback, transfer,
      dev->int_in_endpoint->bInterval);

  return urb_ptr;
}

static void femodule_free_transfers(struct usb_femodule *const dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return;
  }
#endif
  if (dev->isoc_in_transfer) {
    for (int i = 0; i < dev->isoc_in_transfer_count; ++i)
      usb_free_urb(dev->isoc_in_transfer[i].urb);
    kfree(dev->isoc_in_transfer);
    dev->isoc_in_transfer = (struct femodule_transfer *)NULL;
  }
  if (dev->int_in_transfer) {
    for (int i = 0; i < dev->int_in_transfer_count; ++i)
      usb_free_urb(dev->int_in_transfer[i].urb);
    kfree(dev->int_in_transfer);
    dev->int_in_transfer = (struct femodule_transfer *)NULL;
  }
  if (dev->isoc_in_transfer == NULL && dev->int_in_transfer == NULL &&
      dev->workqueue != NULL) {
    destroy_workqueue(dev->workqueue);
    dev->workqueue = (struct workqueue_struct *)NULL;
  }
}

static void femodule_init_transfers(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return;
  }
#endif
  if (dev->workqueue == NULL) {
    dev->workqueue = alloc_ordered_workqueue("femodule", 0);
    if (!dev->workqueue) {
      DBG_CRIT("Failed to allocate workqueue");
      goto fail_free;
    }
  }
  if (dev->isoc_in_endpoint) {
    const int packetSize = bufferSizeForEndpoint(dev->isoc_in_endpoint);
    const int packetsPerTransfer = FEMODULE_ISOC_IN_TRANSFER_SIZE / packetSize;

    dev->isoc_in_transfer =
        kzalloc(sizeof(struct femodule_transfer) * FEMODULE_ISOC_IN_URB_COUNT,
                GFP_KERNEL);
    if (!dev->isoc_in_transfer) {
      DBG_CRIT("Failed to allocate isochronous transfer array");
      goto fail_free;
    }
    dev->isoc_in_transfer_count = FEMODULE_ISOC_IN_URB_COUNT;
    for (int i = 0; i < FEMODULE_ISOC_IN_URB_COUNT; ++i) {
      INIT_WORK(&dev->isoc_in_transfer[i].work, femodule_isoc_in_work);
      dev->isoc_in_transfer[i].dev = dev;
      dev->isoc_in_transfer[i].urb =
          femodule_init_isoc_urb(&dev->isoc_in_transfer[i], packetsPerTransfer);
      if (!dev->isoc_in_transfer[i].urb) goto fail_free;
    }
  }
  if (dev->int_in_endpoint) {
    dev->int_in_transfer =
        kzalloc(sizeof(struct femodule_transfer) * FEMODULE_INT_IN_URB_COUNT,
                GFP_KERNEL);
    if (!dev->int_in_transfer) {
      DBG_CRIT("Failed to allocate interrupt transfer array");
      goto fail_free;
    }
    dev->int_in_transfer_count = FEMODULE_INT_IN_URB_COUNT;
    for (int i = 0; i < FEMODULE_INT_IN_URB_COUNT; ++i) {
      INIT_WORK(&dev->int_in_transfer[i].work, femodule_int_in_work);
      dev->int_in_transfer[i].dev = dev;
      dev->int_in_transfer[i].urb =
          femodule_init_int_urb(&dev->int_in_transfer[i]);
      if (!dev->int_in_transfer[i].urb) goto fail_free;
    }
  }
  return;
fail_free:
  femodule_free_transfers(dev);
  return;
}

static int femodule_start_transfers(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif

  int retval;

  if (dev->isoc_in_transfer) {
    atomic_set(&dev->running, 1);
    dev->isoc_in_frame_errors = 0;
    dev->isoc_in_frame_empties = 0;
#ifdef __LION
    dev->isoc_in_buffer_discard = 0;
#else
    // Bugs in RadioLynx firmware cause corrupt/stale data in the beginning of
    // the GNSS stream. Discard the first several buffers.
    dev->isoc_in_buffer_discard = 8;
#endif
    mb();
    for (int i = 0; i < dev->isoc_in_transfer_count; ++i) {
      if (dev->isoc_in_transfer[i].urb) {
        retval = usb_submit_urb(dev->isoc_in_transfer[i].urb, GFP_KERNEL);
        if (retval) {
          DBG_ERR("Failed to submit isochronous URB (%d)", retval);
          atomic_set(&dev->running, 0);
          return retval;
        }
      }
    }
  }
  if (dev->int_in_transfer) {
    atomic_set(&dev->running, 1);
    dev->int_in_frame_errors = 0;
    mb();
    for (int i = 0; i < dev->int_in_transfer_count; ++i) {
      if (dev->int_in_transfer[i].urb) {
        retval = usb_submit_urb(dev->int_in_transfer[i].urb, GFP_KERNEL);
        if (retval) {
          DBG_ERR("Failed to submit interrupt URB (%d)", retval);
          atomic_set(&dev->running, 0);
          return retval;
        }
      }
    }
  }
  return 0;
}

#ifdef __LION
static int femodule_verify_max2771_channel(
    struct usb_femodule *dev, const char *max2771_config[MAX2771_CONFIG_SIZE],
    const u16 max2771_config_address[MAX2771_CONFIG_SIZE], u16 channel_index) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif

  int retval, j;
  char *stack_buffer = kzalloc(sizeof(char) * 5, GFP_KERNEL);
  if (!stack_buffer) {
    DBG_ERR("Memory allocation error during MAX2771 programming verification");
    return -1;
  }

  for (int i = 0; i < MAX2771_CONFIG_SIZE; ++i) {
    // Bits 0-7 are always zero
    // Bits 8-11 are the address of the register
    // Bit 12 is one (write transaction)
    // Bits 13-15 are turnaround bits (values are irrelevant)

    // The RadioLion clocks in most-significant-bit first. MAX2771 expects the
    // 4-bit register address to immediately follow 8 zeros. So, shift the
    // register address into the top 4 bits of the 2nd byte.
    stack_buffer[1] = (max2771_config_address[i] << 4) | (1 << 3);

    retval = usb_control_msg(dev->udev,
                             usb_sndctrlpipe(dev->udev,
                                             0),  // Endpoint 0
                             VRQ_SET_REG, VRQT_VENDOR_OUT, channel_index, 4,
                             (void *)stack_buffer, 2, 0);
    msleep(50);

    // wIndexL = 4 => in_octets
    // wLengthL = 2 => out_octets

    if (retval < 0) {
      DBG_ERR("Error querying MAX2771-%u (%d)", channel_index, -retval);
      kfree(stack_buffer);
      return retval;
    }

    // wValue must equal channel_index
    // wIndexL must equal out_octets
    // wLengthL must equal in_octets+1

    retval = usb_control_msg(dev->udev,
                             usb_sndctrlpipe(dev->udev,
                                             0),  // Endpoint 0
                             VRQ_SET_REG, VRQT_VENDOR_IN, channel_index, 2,
                             (void *)stack_buffer, 5, 0);
    msleep(50);

    static const char equal_str[] = "equal";
    static const char nequal_str[] = "not equal";
    const char *comp_str = equal_str;
    for (j = 0; j < 4; ++j) {
      if (stack_buffer[j + 1] != max2771_config[i][3 - j]) {
        comp_str = nequal_str;
        break;
      }
    }

    DBG_DEBUG(
        "chip %u, register 0x%01X, wrote: 0x%02X%02X%02X%02X, read: "
        "0x%02X%02X%02X%02X (%s)",
        channel_index, max2771_config_address[i], max2771_config[i][3] & 0xff,
        max2771_config[i][2] & 0xff, max2771_config[i][1] & 0xff,
        max2771_config[i][0] & 0xff, stack_buffer[1] & 0xff,
        stack_buffer[2] & 0xff, stack_buffer[3] & 0xff, stack_buffer[4] & 0xff,
        comp_str);
  }

  kfree(stack_buffer);
  return 0;
}

static int femodule_program_max2771_channel(
    struct usb_femodule *dev, const char *max2771_config[MAX2771_CONFIG_SIZE],
    const u16 max2771_config_address[MAX2771_CONFIG_SIZE], u16 channel_mask) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif

  int retval, j;
  char *stack_buffer = kzalloc(sizeof(char) * 6, GFP_KERNEL);
  if (!stack_buffer) {
    DBG_ERR("Memory allocation error during MAX2771 programming");
    return -1;
  }

  for (int i = 0; i < MAX2771_CONFIG_SIZE; ++i) {
    // Bits 0-7 are always zero
    // Bits 8-11 are the address of the register
    // Bit 12 is zero (write transaction)
    // Bits 13-15 are turnaround bits (values are irrelevant)

    // The RadioLion clocks in most-significant-bit first. MAX2771 expects the
    // 4-bit register address to immediately follow 8 zeros. So, shift the
    // register address into the top 4 bits of the 2nd byte.
    stack_buffer[1] = max2771_config_address[i] << 4;

    // The RadioLion clocks in first-byte-first (unlike on the RadioLynx which
    // clocks in last-byte-first). Reverse register bytes to account for that.
    for (j = 0; j < 4; ++j) stack_buffer[j + 2] = max2771_config[i][3 - j];

    retval = usb_control_msg(dev->udev,
                             usb_sndctrlpipe(dev->udev,
                                             0),  // Endpoint 0
                             VRQ_SET_REG, VRQT_VENDOR_OUT, channel_mask, 0,
                             (void *)stack_buffer, 6, 0);
    msleep(50);

    if (retval < 0) {
      DBG_ERR("Error programming MAX2771-%u (%d)", channel_mask, -retval);
      kfree(stack_buffer);
      return retval;
    }
  }

  kfree(stack_buffer);

#ifdef __DEBUG_DEVICE
  for (int channel_index = 0; channel_index < 16; ++channel_index)
    if (channel_mask & (1 << channel_index))
      femodule_verify_max2771_channel(dev, max2771_config,
                                      max2771_config_address, channel_index);
#endif

  return 0;
}

static u64 bound(u64 val, const u64 min, const u64 max) {
  if (val < min) return min;
  if (val > max) return max;
  return val;
}

// Configure the MAX2771 Local Oscillator (LO) synthesizer as appropriate for
// the given carrier center, TCXO, and sample frequencies. With baseband==true,
// the LO will be configured to mix fC to baseband. With baseband==false, the LO
// will be configured to mix fC to fSamp / 4.
static int configure_synthesizer_max2771(
    unsigned fC, u64 fTCXO_num, u64 fTCXO_den, u64 fSamp_num, u64 fSamp_den,
    s32 del_fIF, bool baseband, struct MAX2771_CONF_ONE_t *max2771_CONF_ONE,
    struct MAX2771_PLL_CONF_t *max2771_PLL_CONF,
    struct MAX2771_N_R_DIV_t *max2771_N_R_DIV,
    struct MAX2771_F_DIV_t *max2771_F_DIV, s32 force_RDIV) {
  if (fTCXO_num < 8000000 * fTCXO_den || fTCXO_num > 44000000 * fTCXO_den) {
    DBG_ERR("Synthesizer requires a reference frequency between 8-44MHz");
    return -1;
  }

#define kHz 1000
#define MHz 1000000
  {
    u64 fLO = fC - del_fIF;
    if (baseband == false) fLO -= fSamp_num / (4 * fSamp_den);
    if (fLO >= 1525 * MHz && fLO <= 1610 * MHz) {
      // High band mixer enabled
      max2771_CONF_ONE->MIXERMODE = 0x0;
      // Local Oscillator band selection: L1
      max2771_PLL_CONF->LOBAND = 0x0;
    } else if (fLO >= 1160 * MHz && fLO <= 1290 * MHz) {
      // Low band mixer enabled
      max2771_CONF_ONE->MIXERMODE = 0x1;
      // Local Oscillator band selection: L2/L5
      max2771_PLL_CONF->LOBAND = 0x1;
    } else {
      DBG_ERR("Unsupported LO frequency");
      return -1;
    }
  }

  // fCOMP = fTCXO / RDIV
  //
  // RDIV is a 10-bit value with a valid range between 1 and 1023.
  // Further, RDIV must be selected to produce an fCOMP between 0.05MHz and
  // 32MHz.
  //
  // Calculate the range of RDIV values that satisfies both restrictions.
  u64 RDIV_max = bound(fTCXO_num / (fTCXO_den * 50 * kHz), 1, 1023);
  u64 RDIV_min = fTCXO_num / (fTCXO_den * 32 * MHz);
  if (fTCXO_num % (fTCXO_den * 32 * MHz)) ++RDIV_min;
  RDIV_min = bound(RDIV_min, 1, 1023);
#undef kHz
#undef MHz

  u64 RDIV_best, NDIV_best, FDIV_best, FDIV_denom_best;
  u64 FDIV_error_best = ~(u64)0;

  // Iterate over all permissible RDIV values
  u64 RDIV;
  if (force_RDIV > 0) {
    RDIV = force_RDIV;
    if (RDIV_max >= RDIV) RDIV_max = RDIV;
    if (RDIV_min <= RDIV) RDIV_min = RDIV;
  } else {
    RDIV = RDIV_min;
  }
  for (; RDIV >= RDIV_min && RDIV <= RDIV_max; ++RDIV) {
    u64 numer, denom;
    if (baseband) {
      // When configuring for baseband sampling, the LO is configured to mix the
      // desired center frequency to baseband plus some delta.
      numer = (fC - del_fIF) * fTCXO_den * RDIV;
      denom = fTCXO_num;
    } else {
      // When configuring for real sampling, the LO is configured to mix the
      // desired center frequency to an IF of fSamp/4 plus some delta.
      numer = (fC - del_fIF) * fTCXO_den * RDIV * fSamp_den * 4 -
              fSamp_num * fTCXO_den * RDIV;
      denom = fTCXO_num * fSamp_den * 4;
    }
    u64 NDIV = numer / denom;

    // Permissible integer portion main divider range is from 36 to 32767
    if (NDIV < 36 || NDIV > 32767) continue;

    const u64 NDIV_error = numer % denom;

    numer = NDIV_error * (1L << 20);
    u64 FDIV = numer / denom;
    u64 FDIV_error = numer % denom;

    const u64 error_best = FDIV_error_best * denom;

    // The FDIV calculation implicitly provides the floor() of a non-integer
    // FDIV. This candidate is evaluated in the first iteration. The ceil()
    // candidate is evaluated in the second iteration.
    for (int iter = 0; iter < 2; ++iter) {
      if (FDIV_error_best == ~(u64)0 ||
          FDIV_error * FDIV_denom_best < error_best) {
        // In the fractional mode, the synthesizer should not be operated with
        // integer division ratios greater than 251.
        if (FDIV == 0 || NDIV <= 251) {
          RDIV_best = RDIV;
          NDIV_best = NDIV;
          FDIV_best = FDIV;
          FDIV_error_best = FDIV_error;
          FDIV_denom_best = denom;
          if (FDIV_error == 0) goto accept_best_candidate;
        }
      }

      // Prepare the ceil() candidate for the second iteration.
      if (++FDIV >= (1L << 20)) {
        FDIV -= (1L << 20);
        if (++NDIV > 32767) break;
      }
      FDIV_error = denom - FDIV_error;
    }
  }

  if (FDIV_error_best == ~(u64)0) {
    DBG_ERR("No feasible synthesizer configurations found");
    return -1;
  }

accept_best_candidate:
  max2771_N_R_DIV->RDIV = RDIV_best;
  max2771_N_R_DIV->NDIV = NDIV_best;
  max2771_F_DIV->FDIV = FDIV_best;

  // Only use Fractional-N mode if required.
  max2771_PLL_CONF->INT_PLL = (max2771_F_DIV->FDIV == 0);

  // In Integer-N mode, if the integer division ratio is divisible by 32, the
  // PLL can operate in a power-save mode.
  max2771_PLL_CONF->PWRSAV =
      (max2771_PLL_CONF->INT_PLL == 1) && ((max2771_N_R_DIV->NDIV % 32) == 0);

  // Double-sided Nyquist bandwidth for real sampling. Single-sided Nyquist
  // bandwith for complex sampling.
  u64 BW = fSamp_num / (fSamp_den * 2);
  // Complex baseband sampling will employ a Lowpass filter. Real IF sampling
  // will employ a complex bandpass filter.
  if (baseband)
    max2771_CONF_ONE->FCENX = 0;  // Lowpass filter
  else
    max2771_CONF_ONE->FCENX = 1;  // Complex bandpass filter
  max2771_CONF_ONE->F3OR5 = 0;    // 5th order

  max2771_CONF_ONE->FCEN = 0;
  if (max2771_CONF_ONE->FCENX == 0) {
    // Lowpass filter
    //
    // Configure the 3dB single-sided bandwidth of the filter to the widest
    // bandwidth that will contain the sampling bandwidth.
    if (BW >= 18000000) {
      max2771_CONF_ONE->FBW = 0b100;
    } else if (BW >= 11700000) {
      max2771_CONF_ONE->FBW = 0b011;
    } else {  // if (BW >= 8200000) {
      max2771_CONF_ONE->FBW = 0b111;
    }
  } else {
    // Complex bandpass filter
    //
    // Configure the 3dB double-sided bandwidth of the filter to the widest
    // bandwidth that will contain the sampling bandwidth. Also program the
    // center frequency of the complex bandpass filter, if enabled. NOTE: The
    // filter center frequency will aim for the center of the Nyquist bandwidth
    // and not for the configured IF (e.g., if shifted appreciably by del_fIF).
    //
    if (BW >= 8700000) {
      max2771_CONF_ONE->FBW = 0b001;
      max2771_CONF_ONE->FCEN = 128 - fSamp_num / (2 * fSamp_den * 660000);
    } else if (BW >= 4200000) {
      max2771_CONF_ONE->FBW = 0b010;
      max2771_CONF_ONE->FCEN = 128 - fSamp_num / (2 * fSamp_den * 355000);
    } else {  // if (BW <= 2500000) {
      max2771_CONF_ONE->FBW = 0b000;
      max2771_CONF_ONE->FCEN = 128 - fSamp_num / (2 * fSamp_den * 195000);
    }
  }

  // Since floating-point math cannot generally be performed in the kernel,
  // relevant parameters are logged to allow hand calculation of the achieved
  // LO frequency by:
  //
  // fCOMP = fTCXO / RDIV;
  //
  // fLO_actual = fCOMP * (NDIV + FDIV / 1048576);
  //
  // And the actual intermediate frequency by:
  //
  // fIF_actual = fC - fLO_actual;
  //
  DBG_DEBUG(
      "fC: %u fTCXO: %llu/%llu fS: %llu/%llu RDIV: %u NDIV: %u FDIV: %u FCENX: "
      "%u FCEN: %u FBW: %u",
      fC, (long long unsigned)fTCXO_num, (long long unsigned)fTCXO_den,
      (long long unsigned)fSamp_num, (long long unsigned)fSamp_den,
      max2771_N_R_DIV->RDIV, max2771_N_R_DIV->NDIV, max2771_F_DIV->FDIV,
      max2771_CONF_ONE->FCENX, max2771_CONF_ONE->FCEN, max2771_CONF_ONE->FBW);

  return 0;
}

// Quick (as in, to implement) Greatest Common Divisor calculation
static u64 gcd(u64 a, u64 b) {
  while (b != 0) {
    const u64 r = a % b;
    a = b;
    b = r;
  }
  return a;
}

static int configure_refclk_max2771(u64 fTCXO_num, u64 fTCXO_den,
                                    u64 *ptr_fRef_num, u64 *ptr_fRef_den,
                                    struct MAX2771_PLL_CONF_t *max2771_PLL_CONF,
                                    struct MAX2771_CLK_ONE_t *max2771_CLK_ONE,
                                    struct MAX2771_CLK_TWO_t *max2771_CLK_TWO) {
  const u64 fRef_num = *ptr_fRef_num;
  const u64 fRef_den = *ptr_fRef_den;
  if (fTCXO_num > 44000000 * fTCXO_den) {
    DBG_ERR("TCXO frequency cannot exceed 44MHz");
    return -1;
  }

  if (fRef_num > 44000000 * fRef_den) {
    DBG_ERR("Reference frequency cannot exceed 44MHz");
    return -1;
  }

  // The output of the reference clock fractional divider need not be verified
  // <= 44MHz as it can only be less than fTCXO.

  // CLKOUT selection: Integer divider multiplier/output
  max2771_CLK_TWO->CLKOUT_SEL = 0x0;

  // The ADC will use the externally supplied ADC_CLKIN
  max2771_CLK_ONE->EXTADCCLK = 0x1;

  // Each element of these arrays corresponds index-wise with the value of
  // REFDIV for the integer multiplier/divider.
  //
  // Suppose the integer multiplier/divider is configured for x2. The
  // relationship between the input and output frequencies is:
  //
  // 2 * fTCXO_num / fTCXO_den == fRef_num / fRef_den
  //
  // Note that the denominators are multiplied through to eliminate division.
  // So, the ratio fOUT/fIN is correct but their specific values are scaled by
  // a common factor.
  //
  const u64 fIN[5] = {2 * fTCXO_num * fRef_den, fTCXO_num * fRef_den,
                      fTCXO_num * fRef_den, fTCXO_num * fRef_den,
                      4 * fTCXO_num * fRef_den};
  const u64 fOUT[5] = {fRef_num * fTCXO_den, 4 * fRef_num * fTCXO_den,
                       2 * fRef_num * fTCXO_den, fRef_num * fTCXO_den,
                       fRef_num * fTCXO_den};
  const bool feasible_int[5] = {fTCXO_num <= 22000000 * fTCXO_den, true, true,
                                true, fTCXO_num <= 11000000 * fTCXO_den};

  // Check if the integer multiplier/divider is sufficient alone without
  // involving the reference clock fractional divider.
  for (int i = 0; i < 5; ++i) {
    if (feasible_int[i] && fIN[i] == fOUT[i]) {
      max2771_CLK_TWO->PREFRACDIV_SEL = 0x0;
      max2771_PLL_CONF->REFDIV = i;
      return 0;
    }
  }

  // Otherwise, the reference clock fractional divider will be used in
  // addition to the integer multiplier/divider.
  max2771_CLK_TWO->PREFRACDIV_SEL = 0x1;

  // The equation governing the behavior of the fractional frequency divider
  // is:
  //
  // fOUT / fIN = LCOUNT / (4096 - MCOUNT + LCOUNT)
  //
  // The following calculations solve for LCOUNT as a function of MCOUNT,
  // fOUT, and fIN.

  // fOUT / fIN cannot exceed 0.5 for the fractional frequency divider
  const bool feasible_frac[5] = {2 * fOUT[0] <= fIN[0], 2 * fOUT[1] <= fIN[1],
                                 2 * fOUT[2] <= fIN[2], 2 * fOUT[3] <= fIN[3],
                                 2 * fOUT[4] <= fIN[4]};
  u64 MCOUNTS[5] = {0, 0, 0, 0, 0};
  u64 LCOUNTS[5] = {0, 0, 0, 0, 0};
  u64 LCOUNTS_error[5] = {~(u64)0, ~(u64)0, ~(u64)0, ~(u64)0, ~(u64)0};

  // Iterate through all feasible integer multiplier/divider settings
  for (int i = 0; i < 5; ++i) {
    if (feasible_int[i] == false || feasible_frac[i] == false) continue;

    const u64 denom = fIN[i] - fOUT[i];

    // Calculate LCOUNT as a function of MCOUNT and keep the best one
    for (u64 MCOUNT = 0; MCOUNT < 4096; ++MCOUNT) {
      const u64 numer = (4096 - MCOUNT) * fOUT[i];
      // Calculate a candidate LCOUNT using integer algebra. The 'error'
      // associated with the candidate is the division remainder.
      u64 LCOUNT = numer / denom;
      u64 LCOUNT_error = numer % denom;

      // The LCOUNT calculation implicitly provides the floor() of a non-integer
      // LCOUNT. This candidate is evaluated in the first iteration. The ceil()
      // candidate is evaluated in the second iteration.
      for (int iter = 0; iter < 2; ++iter) {
        if (LCOUNT < 4096 && LCOUNT_error < LCOUNTS_error[i]) {
          LCOUNTS_error[i] = LCOUNT_error;
          MCOUNTS[i] = MCOUNT;
          LCOUNTS[i] = LCOUNT;
          if (LCOUNTS_error[i] == 0) goto compare_candidates;
        }

        // Prepare the ceil() candidate for the second iteration.
        ++LCOUNT;
        LCOUNT_error = denom - LCOUNT_error;
      }
    }
  }

compare_candidates:
  // Compare all LCOUNT/MCOUNT candidates pairwise. The largest in each
  // pairwise comparison is eliminated.
  for (int i = 0; i < 4; ++i) {
    if (feasible_int[i] == false || feasible_frac[i] == false) continue;

    const u64 denom_i = fIN[i] - fOUT[i];
    for (int j = i + 1; j < 5; ++j) {
      if (feasible_int[j] == false || feasible_frac[j] == false) continue;

      const u64 denom_j = fIN[j] - fOUT[j];
      if (LCOUNTS_error[i] * denom_j < LCOUNTS_error[j] * denom_i) {
        // i has a smaller error than j. Eliminate the j-candidate.
        LCOUNTS_error[j] = ~(u64)0;
      } else {
        // j has a smaller (or equal) error than i. Eliminate the i-candidate.
        LCOUNTS_error[i] = ~(u64)0;
        break;
      }
    }
  }

  // Finally, search through the LCOUNT/MCOUNT candidates for the only
  // remaining entry.
  for (int i = 0; i < 5; ++i) {
    if (LCOUNTS_error[i] == ~(u64)0) continue;

    const u64 MCOUNT = MCOUNTS[i];
    const u64 LCOUNT = LCOUNTS[i];

    max2771_CLK_ONE->REFCLK_M_CNT = MCOUNT;
    max2771_CLK_ONE->REFCLK_L_CNT = LCOUNT;
    max2771_PLL_CONF->REFDIV = i;

    // Return the achieved reference frequency
    *ptr_fRef_num = fTCXO_num * LCOUNT;
    *ptr_fRef_den = fTCXO_den * (4096 - MCOUNT + LCOUNT);
    switch (i) {
      case 0b000:
        *ptr_fRef_num *= 2;
        break;
      case 0b001:
        *ptr_fRef_den *= 4;
        break;
      case 0b010:
        *ptr_fRef_den *= 2;
        break;
      case 0b011:
        break;
      case 0b100:
        *ptr_fRef_num *= 4;
        break;
    }

    // Reduce the reference frequency fraction
    const u64 gcd_fRef = gcd(*ptr_fRef_num, *ptr_fRef_den);
    *ptr_fRef_num /= gcd_fRef;
    *ptr_fRef_den /= gcd_fRef;

    DBG_DEBUG(
        "fTCXO: %llu/%llu fRef: %llu/%llu LCOUNT: %llu MCOUNT: %llu REFDIV: %u",
        (long long unsigned)fTCXO_num, (long long unsigned)fTCXO_den,
        (long long unsigned)fRef_num, (long long unsigned)fRef_den,
        (long long unsigned)LCOUNT, (long long unsigned)MCOUNT,
        max2771_PLL_CONF->REFDIV);

    return 0;
  }

  // No feasible solutions were found.
  DBG_ERR("No feasible reference clock configurations found");
  return -1;
}

static int femodule_program_max2771(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif

  // Make local copies of MAX2771 default register values for those registers
  // that will be modified.
  struct MAX2771_CONF_ONE_t max2771_CONF_ONE = MAX2771_CONF_ONE_default;
  struct MAX2771_CONF_TWO_t max2771_CONF_TWO = MAX2771_CONF_TWO_default;
  struct MAX2771_CONF_THREE_t max2771_CONF_THREE = MAX2771_CONF_THREE_default;
  struct MAX2771_PLL_CONF_t max2771_PLL_CONF = MAX2771_PLL_CONF_default;
  struct MAX2771_N_R_DIV_t max2771_N_R_DIV = MAX2771_N_R_DIV_default;
  struct MAX2771_F_DIV_t max2771_F_DIV = MAX2771_F_DIV_default;
  struct MAX2771_CLK_ONE_t max2771_CLK_ONE = MAX2771_CLK_ONE_default;
  struct MAX2771_CLK_TWO_t max2771_CLK_TWO = MAX2771_CLK_TWO_default;

  const char *max2771_config[MAX2771_CONFIG_SIZE] = {
      (char *)&max2771_CONF_ONE,         (char *)&max2771_CONF_TWO,
      (char *)&max2771_CONF_THREE,       (char *)&max2771_PLL_CONF,
      (char *)&max2771_N_R_DIV,          (char *)&max2771_F_DIV,
      (char *)&MAX2771_RESERVED_default, (char *)&max2771_CLK_ONE,
      (char *)&MAX2771_TEST_ONE_default, (char *)&MAX2771_TEST_TWO_default,
      (char *)&max2771_CLK_TWO};

  const u16 max2771_config_address[MAX2771_CONFIG_SIZE] = {
      0x0,  // CONF_ONE
      0x1,  // CONF_TWO
      0x2,  // CONF_THREE
      0x3,  // PLL_CONF
      0x4,  // N_R_DIV
      0x5,  // F_DIV
      0x6,  // RESERVED
      0x7,  // CLK_ONE
      0x8,  // TEST_ONE
      0x9,  // TEST_TWO
      0xA,  // CLK_TWO
  };

  int retval;

  // Disable both LNAs
  max2771_CONF_ONE.LNAMODE = 0x2;

  // See Figure 3: Clock Distribution of the MAX2771 datasheet. The following
  // function configures the reference clock section to achieve the desired
  // REFERENCE_FREQUENCY from the input TCXO_FREQUENCY. It then provides
  // that reference clock to CLK_OUT which is distributed through an external
  // CPLD back to ADC_CLKIN of all MAXIM chips (including this one).
  //
  // Note that the PLL reference clock (which provides the LO to the mixer)
  // comes directly from XTAL.
  //
  u64 referenceFrequencyNumerator = REFERENCE_FREQUENCY_NUMERATOR;
  u64 referenceFrequencyDenominator = REFERENCE_FREQUENCY_DENOMINATOR;

  retval = configure_refclk_max2771(
      TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      &referenceFrequencyNumerator, &referenceFrequencyDenominator,
      &max2771_PLL_CONF, &max2771_CLK_ONE, &max2771_CLK_TWO);
  if (retval < 0) return retval;

  // Specify whether to enable complex baseband sampling on each frequency band.
  const bool complexL1 = false, complexL2 = false, complexL5 = false;

  // Specify an amount (in Hz) by which to adjust the desired IF (intermediate
  // frequency). This value should be larger than the maximum expected signal
  // Doppler to ensure all signals remain comfortably away from fS/4 (real IF
  // samples) or 0 (complex baseband samples).
  const s32 del_fIF_pri = 50000;
  const s32 del_fIF_alt = del_fIF_pri + 30000;

  // Use output of reference clock divider/multiplier
  max2771_CLK_ONE.ADCCLK = 0x0;
  // Bypass ADC clock divider
  max2771_CLK_ONE.FCLKIN = 0x0;

  // PRIMARY L1
  //

  // Enable CLKOUT output clock buffer
  max2771_PLL_CONF.REFOUTEN = 0x1;

  // Frequency of the GPS L1 carrier (in Hz)
  const int fL1 = 1575420000;

  // Enable quadrature sampling, if applicable.
  max2771_CONF_TWO.IQEN = complexL1;
  max2771_CONF_THREE.PGAQEN = complexL1;

  const u64 sampleFrequencyL1Numerator =
      referenceFrequencyNumerator * SAMPLE_FREQUENCY_RATIO_NUMERATOR;
  const u64 sampleFrequencyL1Denominator =
      referenceFrequencyDenominator * SAMPLE_FREQUENCY_RATIO_DENOMINATOR;
  retval = configure_synthesizer_max2771(
      fL1, TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      sampleFrequencyL1Numerator, sampleFrequencyL1Denominator, del_fIF_pri,
      complexL1, &max2771_CONF_ONE, &max2771_PLL_CONF, &max2771_N_R_DIV,
      &max2771_F_DIV,
#ifdef FORCE_L1_RDIV
      FORCE_L1_RDIV
#else
      -1
#endif
  );
  if (retval < 0) return retval;

  retval = femodule_program_max2771_channel(dev, max2771_config,
                                            max2771_config_address, 0b000001);
  if (retval < 0) return retval;

  // SECONDARY L1
  //

  retval = configure_synthesizer_max2771(
      fL1, TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      sampleFrequencyL1Numerator, sampleFrequencyL1Denominator, del_fIF_alt,
      complexL1, &max2771_CONF_ONE, &max2771_PLL_CONF, &max2771_N_R_DIV,
      &max2771_F_DIV,
#ifdef FORCE_L1_RDIV
      FORCE_L1_RDIV
#else
      -1
#endif
  );
  if (retval < 0) return retval;

  // Disable CLKOUT output clock buffer
  max2771_PLL_CONF.REFOUTEN = 0x0;

  retval = femodule_program_max2771_channel(dev, max2771_config,
                                            max2771_config_address, 0b001000);
  if (retval < 0) return retval;

  // PRIMARY L2
  //

  // Frequency of the GPS L2 carrier (in Hz)
  const int fL2 = 1227600000;

  // Enable quadrature sampling, if applicable.
  max2771_CONF_TWO.IQEN = complexL2;
  max2771_CONF_THREE.PGAQEN = complexL2;

  const u64 sampleFrequencyL2Numerator =
      referenceFrequencyNumerator * SAMPLE_FREQUENCY_L2_RATIO_NUMERATOR;
  const u64 sampleFrequencyL2Denominator =
      referenceFrequencyDenominator * SAMPLE_FREQUENCY_L2_RATIO_DENOMINATOR;

  retval = configure_synthesizer_max2771(
      fL2, TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      sampleFrequencyL2Numerator, sampleFrequencyL2Denominator, del_fIF_pri,
      complexL2, &max2771_CONF_ONE, &max2771_PLL_CONF, &max2771_N_R_DIV,
      &max2771_F_DIV,
#ifdef FORCE_L2_RDIV
      FORCE_L2_RDIV
#else
      -1
#endif
  );
  if (retval < 0) return retval;

  retval = femodule_program_max2771_channel(dev, max2771_config,
                                            max2771_config_address, 0b000010);
  if (retval < 0) return retval;

  // SECONDARY L2
  //

  retval = configure_synthesizer_max2771(
      fL2, TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      sampleFrequencyL2Numerator, sampleFrequencyL2Denominator, del_fIF_alt,
      complexL2, &max2771_CONF_ONE, &max2771_PLL_CONF, &max2771_N_R_DIV,
      &max2771_F_DIV,
#ifdef FORCE_L2_RDIV
      FORCE_L2_RDIV
#else
      -1
#endif
  );
  if (retval < 0) return retval;

  retval = femodule_program_max2771_channel(dev, max2771_config,
                                            max2771_config_address, 0b010000);
  if (retval < 0) return retval;

  // PRIMARY L5
  //

  // Frequency of the GPS L5 carrier (in Hz)
  const int fL5 = 1176450000;

  // Enable quadrature sampling, if applicable.
  max2771_CONF_TWO.IQEN = complexL5;
  max2771_CONF_THREE.PGAQEN = complexL5;

  const u64 sampleFrequencyL5Numerator =
      referenceFrequencyNumerator * SAMPLE_FREQUENCY_L5_RATIO_NUMERATOR;
  const u64 sampleFrequencyL5Denominator =
      referenceFrequencyDenominator * SAMPLE_FREQUENCY_L5_RATIO_DENOMINATOR;

  retval = configure_synthesizer_max2771(
      fL5, TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      sampleFrequencyL5Numerator, sampleFrequencyL5Denominator, del_fIF_pri,
      complexL5, &max2771_CONF_ONE, &max2771_PLL_CONF, &max2771_N_R_DIV,
      &max2771_F_DIV,
#ifdef FORCE_L5_RDIV
      FORCE_L5_RDIV
#else
      -1
#endif
  );
  if (retval < 0) return retval;

  retval = femodule_program_max2771_channel(dev, max2771_config,
                                            max2771_config_address, 0b000100);
  if (retval < 0) return retval;

  // SECONDARY L5
  //

  retval = configure_synthesizer_max2771(
      fL5, TCXO_FREQUENCY_NUMERATOR, TCXO_FREQUENCY_DENOMINATOR,
      sampleFrequencyL5Numerator, sampleFrequencyL5Denominator, del_fIF_alt,
      complexL5, &max2771_CONF_ONE, &max2771_PLL_CONF, &max2771_N_R_DIV,
      &max2771_F_DIV,
#ifdef FORCE_L5_RDIV
      FORCE_L5_RDIV
#else
      -1
#endif
  );
  if (retval < 0) return retval;

  retval = femodule_program_max2771_channel(dev, max2771_config,
                                            max2771_config_address, 0b100000);
  if (retval < 0) return retval;

  return 0;
}
#else
static int femodule_program_max2769(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif

  int retval, j;
  char *stack_buffer = kmalloc(sizeof(char) * 4, GFP_KERNEL);
  if (!stack_buffer) {
    DBG_ERR("Memory allocation error during MAX2769 programming");
    return -1;
  }

  for (int i = 0; i < max2769_config_size; ++i) {
    for (j = 0; j < 4; ++j) stack_buffer[j] = max2769_config[i][j];

    retval = usb_control_msg(dev->udev,
                             usb_sndctrlpipe(dev->udev,
                                             0),  // Endpoint 0
                             VRQ_SET_REG, VRQT_VENDOR_OUT, 0, 0,
                             (void *)stack_buffer, 4, 0);
    msleep(50);

    if (retval < 0) {
      DBG_ERR("Error programming MAX2769 (%d)", -retval);
      kfree(stack_buffer);
      return retval;
    }
  }

  kfree(stack_buffer);
  return 0;
}

static int femodule_program_adf4360(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif

  int retval, j;
  char *stack_buffer = kmalloc(sizeof(char) * 3, GFP_KERNEL);
  if (!stack_buffer) {
    DBG_ERR("Memory allocation error during ADF4360 programming");
    return -1;
  }

  for (int i = 0; i < adf4360_config_size; ++i) {
    for (j = 0; j < 3; ++j) stack_buffer[j] = adf4360_config[i][j];

    retval = usb_control_msg(dev->udev,
                             usb_sndctrlpipe(dev->udev,
                                             0),  // Endpoint 0
                             VRQ_SET_PLL, VRQT_VENDOR_OUT, 0, 0,
                             (void *)stack_buffer, 3, 0);
    msleep(50);

    if (retval < 0) {
      DBG_ERR("Error programming ADF4360 (%d)", -retval);
      kfree(stack_buffer);
      return retval;
    }
  }

  kfree(stack_buffer);
  return 0;
}
#endif

static void femodule_abort_transfers(struct usb_femodule *const dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_CRIT("dev is NULL");
    return;
  }
#endif

  if (dev->isoc_in_transfer) {
    atomic_set(&dev->running, 0);
    mb();
    for (int i = 0; i < dev->isoc_in_transfer_count; ++i) {
      usb_kill_urb(dev->isoc_in_transfer[i].urb);
      flush_work(&dev->isoc_in_transfer[i].work);
    }
  }
  if (dev->int_in_transfer) {
    atomic_set(&dev->running, 0);
    mb();
    for (int i = 0; i < dev->int_in_transfer_count; ++i) {
      usb_kill_urb(dev->int_in_transfer[i].urb);
      flush_work(&dev->int_in_transfer[i].work);
    }
  }
}

static int femodule_start_stream(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif
  if (dev->isoc_in_endpoint) {
    DBG_INFO("Starting GNSS stream");
    /* Command Device to commence GNSS stream. */
    int retval =
        usb_control_msg(dev->udev,
                        usb_sndctrlpipe(dev->udev,
                                        0),  // Endpoint 0
                        VRQ_START_XFER, VRQT_VENDOR_OUT, 0, 0, NULL, 0, 0);
    msleep(50);
    if (retval < 0) {
      DBG_ERR("Failed to send USB control message (%d)", retval);
      return -1;
    }
  }
  if (dev->int_in_endpoint) {
    // Introduce initial ReportImuConfig to IMU stream
    femodule_imuconfig_fill_commit(dev);
    femodule_commit_to_output_queue(
        &dev->imu_output_queue, imuconfig_ptr,
        sizeof(struct femodule_reportimuconfig_struct));
  }
  return 0;
}

static int femodule_stop_stream(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return -1;
  }
#endif
  if (dev->isoc_in_endpoint) {
    DBG_INFO("Stopping GNSS stream");
    if (dev->udev) {
      /* Command Device to halt GNSS stream. */
      int retval =
          usb_control_msg(dev->udev,
                          usb_sndctrlpipe(dev->udev,
                                          0),  // Endpoint 0
                          VRQ_STOP_XFER, VRQT_VENDOR_OUT, 0, 0, NULL, 0, 0);
      msleep(50);
      if (retval < 0)
        DBG_ERR("Failed to send USB control message (%d)", retval);
    }
  }
  return 0;
}

static inline char *femodule_request_output(struct oq_top *oq,
                                            size_t *count_request) {
#ifdef __DEBUG_PROTECT
  if (!oq) {
    DBG_DEBUG("oq is NULL");
    return (char *)NULL;
  }
  if (!oq->oqueue) {
    DBG_DEBUG("oqueue is NULL");
    return (char *)NULL;
  }
  if (!count_request) {
    DBG_DEBUG("count_request is NULL");
    return (char *)NULL;
  }
#endif
  const int output_idx = atomic_read(&oq->output_idx) % oq->size;
  const size_t output_oft = oq->output_oft;
  *count_request =
      min(*count_request, oq->oqueue[output_idx].count - output_oft);
  return oq->oqueue[output_idx].ptr + output_oft;
}

static inline void femodule_claim_output(struct oq_top *oq, size_t count) {
#ifdef __DEBUG_PROTECT
  if (!oq) {
    DBG_DEBUG("oq is NULL");
    return;
  }
  if (!oq->oqueue) {
    DBG_DEBUG("oqueue is NULL");
    return;
  }
  if (count <= 0) {
    DBG_DEBUG("count is non-positive");
    return;
  }
#endif
  const int output_idx = atomic_read(&oq->output_idx) % oq->size;
  oq->output_oft += count;
  mb();
#ifdef __DEBUG_PROTECT
  if (oq->output_oft > oq->oqueue[output_idx].count)
    DBG_DEBUG("More output was claimed than provided");
#endif
  if (oq->output_oft >= oq->oqueue[output_idx].count) {
    atomic_inc(&oq->output_idx);
    oq->output_oft = 0;
    mb();
    oq_overrun(oq);
  }
}

static inline bool oq_data_available(struct oq_top *const oq) {
  // != is used in place of < to ensure proper behavior when input_idx
  // invariably overflows before output_idx.
  const bool avail =
      atomic_read(&oq->output_idx) != atomic_read(&oq->input_idx);
  return avail;
}

static ssize_t femodule_read(struct file *file_ptr, char __user *user_buffer,
                             size_t count, loff_t *position) {
  size_t retval = 0;
  struct usb_femodule *const dev = file_ptr->private_data;
  struct oq_top *oq;
  size_t count_pending = count;
  size_t count_written = 0;

  if (dev->read_failure_cached) {
    retval = dev->read_failure_cached;
    dev->read_failure_cached = 0;
    return retval;
  }

  if (!count || !atomic_read(&dev->running) ||
      !atomic_read(&femodule_shared.dev_gnss.running))
    return 0;

  if (dev->isoc_in_endpoint)
    oq = &dev->gnss_output_queue;
  else if (dev->int_in_endpoint)
    oq = &dev->imu_output_queue;
  else
    return -EFAULT;

  do {
    while (atomic_read(&dev->running) &&
           atomic_read(&femodule_shared.dev_gnss.running) &&
           !oq_data_available(oq)) {
      if (file_ptr->f_flags & O_NONBLOCK) {
        if (count_written == 0)
          return -EAGAIN;
        else
          return count_written;
      }
      do {
        retval = wait_event_interruptible(
            oq->wqueue, !atomic_read(&dev->running) ||
                            !atomic_read(&femodule_shared.dev_gnss.running) ||
                            oq_data_available(oq));
      } while (retval == -EINTR);
    }

    if (!atomic_read(&dev->running) ||
        !atomic_read(&femodule_shared.dev_gnss.running))
      return 0;

    const char *output_ptr = femodule_request_output(oq, &count_pending);
    if (!output_ptr) {
#ifdef __DEBUG_PROTECT
      DBG_DEBUG("output_ptr is NULL");
#endif
      return 0;
    }

    const size_t retval =
        copy_to_user(&user_buffer[count_written], output_ptr, count_pending);
    const size_t count_written_now = count_pending - retval;
    femodule_claim_output(oq, count_written_now);
    *position += count_written_now;
    count_written += count_written_now;

    if (retval) {
      if (retval == count_pending) {
        dev->read_failure_cached = 0;
        return -EFAULT;
      } else {
        dev->read_failure_cached = -EFAULT;
        break;
      }
    } else {
      dev->read_failure_cached = 0;
    }

    count_pending = count - count_written;
  } while (count_pending > 0);

  return count_written;
}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 35))
static long femodule_ioctl(struct inode *i, struct file *file, unsigned int cmd,
                           unsigned long arg) {
#else
static long femodule_ioctl(struct file *file, unsigned int cmd,
                           unsigned long arg) {
#endif
  struct usb_femodule *const dev = file->private_data;
  int retval;
  vendor_arg_t *vendor_arg = NULL;
  char *data_buffer = NULL;

#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_ERR("dev is NULL");
    retval = -ENODEV;
    goto exit;
  }
#endif

  /* Lock our device */
  do {
    retval = mutex_lock_interruptible(&dev->mutex);
  } while (retval == -EINTR);
  retval = 0;

#ifdef __DEBUG_PROTECT
  if (!dev->udev) {
    DBG_ERR("udev is NULL");
    retval = -ENODEV;
    goto unlock_exit;
  }
#endif

  if (!dev->open) {
    retval = -EINVAL;
    goto unlock_exit;
  }

  switch (cmd) {
    case FEMODULE_IOCTL_VENDOR_OUT:
      if (!arg) {
        retval = -EINVAL;
        goto unlock_exit;
      }
      vendor_arg = kmalloc(sizeof(vendor_arg_t), GFP_KERNEL);
      if (!vendor_arg) {
        retval = -ENOMEM;
        goto unlock_exit;
      }
      if (copy_from_user(vendor_arg, (const vendor_arg_t *)arg,
                         sizeof(vendor_arg_t))) {
        retval = -EACCES;
        goto unlock_exit;
      }
      // TODO: Should VRQ_STOP_XFER and VRQ_START_XFER vendor requests be
      // allowed to pass through this interface?
      if (vendor_arg->data && vendor_arg->size) {
        // Allocate a buffer in kernel memory to copy user request data into.
        data_buffer = kmalloc(vendor_arg->size, GFP_KERNEL);
        if (!data_buffer) {
          retval = -ENOMEM;
          goto unlock_exit;
        }
        // Copy request data from userspace to kernel.
        if (copy_from_user(data_buffer, vendor_arg->data, vendor_arg->size)) {
          retval = -EACCES;
          goto unlock_exit;
        }
      }
      // Submit the vendor request
      retval =
          usb_control_msg(dev->udev,
                          usb_sndctrlpipe(dev->udev,
                                          0),  // Endpoint 0
                          vendor_arg->request, VRQT_VENDOR_OUT,
                          vendor_arg->value, vendor_arg->index, data_buffer,
                          vendor_arg->size, vendor_arg->timeout);
      msleep(50);
      break;
    case FEMODULE_IOCTL_VENDOR_IN:
      if (!arg) {
        retval = -EINVAL;
        goto unlock_exit;
      }
      vendor_arg = kmalloc(sizeof(vendor_arg_t), GFP_KERNEL);
      if (!vendor_arg) {
        retval = -ENOMEM;
        goto unlock_exit;
      }
      if (copy_from_user(vendor_arg, (const vendor_arg_t *)arg,
                         sizeof(vendor_arg_t))) {
        retval = -EACCES;
        goto unlock_exit;
      }
      // Allocate a buffer in kernel memory to accomodate the control message
      // reply.
      if (vendor_arg->data && vendor_arg->size) {
        data_buffer = kzalloc(vendor_arg->size, GFP_KERNEL);
        if (!data_buffer) {
          retval = -ENOMEM;
          goto unlock_exit;
        }
      }
      // Submit the vendor request
      retval =
          usb_control_msg(dev->udev,
                          usb_sndctrlpipe(dev->udev,
                                          0),  // Endpoint 0
                          vendor_arg->request, VRQT_VENDOR_IN,
                          vendor_arg->value, vendor_arg->index, data_buffer,
                          vendor_arg->size, vendor_arg->timeout);
      msleep(50);
      // Copy the control message reply data from kernel to userspace.
      if (retval >= 0 && data_buffer &&
          copy_to_user(vendor_arg->data, data_buffer, vendor_arg->size))
        retval = -EACCES;
      break;
    default:
      retval = -EINVAL;
      break;
  }

unlock_exit:
  mutex_unlock(&dev->mutex);

exit:
  if (vendor_arg) kfree(vendor_arg);
  if (data_buffer) kfree(data_buffer);
  return retval >= 0 ? 0 : retval;
}

static int femodule_open(struct inode *inode, struct file *file_ptr) {
  struct usb_femodule *dev;
  struct usb_interface *interface;
  int subminor, retval = 0;

  mutex_lock(&disconnect_mutex);

  subminor = iminor(inode);
  interface = usb_find_interface(&femodule_driver, subminor);
  if (!interface) {
    DBG_ERR("Unable to find device for minor %d", subminor);
    retval = -ENODEV;
    goto exit;
  }

  dev = usb_get_intfdata(interface);
  if (!dev) {
    retval = -ENODEV;
    goto exit;
  }

  /* lock this device */
  do {
    retval = mutex_lock_interruptible(&dev->mutex);
  } while (retval == -EINTR);
  retval = 0;

  if (dev->int_in_endpoint && !atomic_read(&femodule_shared.dev_gnss.running)) {
    retval = -EPERM;
    goto unlock_exit;
  }

  if (dev->open) {
    retval = -EPERM;
    goto unlock_exit;
  } else {
    dev->open = true;
    mb();
  }

  DBG_INFO("Opened /dev/" FEMODULE_VARIANT_LCASE "%d",
           subminor - FEMODULE_MINOR_BASE);

  /* Initialize isochronous and interrupt transfers. */
  femodule_init_transfers(dev);
  /* Initialize circular buffers and output queues. */
  femodule_init_circular_buffers(dev);

  /* Start transfer */
  if (femodule_start_transfers(dev)) {
    dev->open = false;
    goto unlock_exit;
  }

  /* Start stream. */
  if (femodule_start_stream(dev)) {
    femodule_abort_transfers(dev);
    dev->open = false;
    goto unlock_exit;
  }

  mb();

  /* Save our object in the file's private structure. */
  file_ptr->private_data = dev;

unlock_exit:
  mutex_unlock(&dev->mutex);

exit:
  mutex_unlock(&disconnect_mutex);
  return retval;
}

static void femodule_cleanup_dev(struct usb_femodule *dev) {
#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return;
  }
  if (dev->open) DBG_DEBUG("Clean-up while still open");
  if (dev->isoc_in_transfer) DBG_DEBUG("Isochronous transfers not freed first");
  if (dev->int_in_transfer) DBG_DEBUG("Interrupt transfers not freed first");
  if (dev->workqueue) DBG_DEBUG("Workqueue not destroyed first");
#endif
  // Disassociate this device with the associated USB interface
  if (dev->interface && usb_get_intfdata(dev->interface) == dev)
    usb_set_intfdata(dev->interface, NULL);
  // If cleaning up the GNSS device then free the stuffing buffer
  if (dev->isoc_in_endpoint && stuffing_ptr) {
    kfree(stuffing_ptr);
    stuffing_ptr = (char *)NULL;
    stuffing_size = 0;
  }
  // If cleaning up the IMU device then free the ReportImuConfig buffer
  if (dev->int_in_endpoint && imuconfig_ptr) {
    kfree(imuconfig_ptr);
    imuconfig_ptr = (char *)NULL;
    imuconfig_ptr = 0;
  }
  /* Delete circular buffers */
  femodule_delete_circular_buffers(dev);
  /* Null out the various data members */
  dev->isoc_in_endpoint = NULL;
  dev->int_in_endpoint = NULL;
  dev->minor = 0;
  atomic_set(&dev->running, 0);
  dev->read_failure_cached = 0;
  dev->isoc_in_frame_errors = 0;
  dev->isoc_in_frame_empties = 0;
  dev->isoc_in_timestamp[0] = 0;
  dev->isoc_in_timestamp[1] = 0;
  dev->isoc_in_timestamp_valid = false;
  dev->isoc_in_tindex = 0;
  dev->isoc_in_buffer_discard = 0;
  dev->int_in_last_config = 0;
}

static int femodule_release(struct inode *inode, struct file *file) {
  struct usb_femodule *dev;
  int retval;

  dev = file->private_data;
  if (!dev) {
    DBG_ERR("dev is NULL");
    retval = -ENODEV;
    goto exit;
  }

  /* Lock our device */
  do {
    retval = mutex_lock_interruptible(&dev->mutex);
  } while (retval == -EINTR);
  retval = 0;

  if (dev->isoc_in_endpoint) DBG_INFO("Released GNSS stream device");

  if (dev->int_in_endpoint) DBG_INFO("Released IMU stream device");

  if (!dev->open) {
    DBG_ERR("Attempted to close an unopened device");
    retval = -ENODEV;
    goto unlock_exit;
  }

  /* Stop GNSS stream */
  femodule_stop_stream(dev);
  /* If releasing the GNSS stream then the IMU reader must also be alerted */
  if (dev->isoc_in_endpoint) femodule_stop_imu_reader();
  /* Abort USB transfers */
  femodule_abort_transfers(dev);
  /* Free USB transfer structures */
  femodule_free_transfers(dev);

  dev->open = false;
  if (!dev->udev) {
    DBG_DEBUG("Device unplugged while still in use");
    femodule_cleanup_dev(dev);
  }

unlock_exit:
  mutex_unlock(&dev->mutex);

  if (dev->isoc_in_endpoint) {
    do {
      retval = mutex_lock_interruptible(&dev->mutex_isoc_in_data);
    } while (retval == -EINTR);

    dev->isoc_in_timestamp_valid = false;

    mutex_unlock(&dev->mutex_isoc_in_data);
  }

exit:
  return retval;
}

static struct file_operations femodule_fops = {
  .owner = THIS_MODULE,
  .read = femodule_read,
  .open = femodule_open,
  .release = femodule_release,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 35))
  .ioctl = femodule_ioctl
#else
  .unlocked_ioctl = femodule_ioctl
#endif
};

static struct usb_class_driver femodule_class = {
    .name = FEMODULE_VARIANT_LCASE "%d",
    .fops = &femodule_fops,
    .minor_base = FEMODULE_MINOR_BASE};

static struct usb_femodule *femodule_setup_dev(
    struct usb_interface *interface) {
  struct usb_femodule *dev = NULL;
  struct usb_device *udev = interface_to_usbdev(interface);
  if (!udev) {
    DBG_ERR("udev is NULL");
    goto exit;
  }
  struct usb_host_interface *iface_desc = interface->cur_altsetting;
  struct usb_endpoint_descriptor *endpoint;

  /* Set up isochronous and interrupt endpoint information. */
  for (int i = 0; i < iface_desc->desc.bNumEndpoints; ++i) {
    endpoint = &iface_desc->endpoint[i].desc;
    switch (endpoint->bEndpointAddress & USB_ENDPOINT_DIR_MASK) {
      case USB_DIR_OUT:
        break;
      case USB_DIR_IN:
        switch (endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) {
          case USB_ENDPOINT_XFER_ISOC:
            femodule_shared.dev_gnss.isoc_in_endpoint = endpoint;
            dev = &femodule_shared.dev_gnss;
            break;
          case USB_ENDPOINT_XFER_INT:
            femodule_shared.dev_imu.int_in_endpoint = endpoint;
            dev = &femodule_shared.dev_imu;
            break;
        }
        break;
    }
  }

  if (!dev) goto exit;

  mutex_init(&dev->mutex);
  mutex_init(&dev->mutex_isoc_in_data);
  mutex_init(&dev->mutex_int_in_data);
  dev->udev = udev;
  dev->interface = interface;
  dev->workqueue = NULL;
  dev->read_failure_cached = 0;
  dev->open = false;

  /* Save our data pointer in this interface device. */
  usb_set_intfdata(interface, dev);

exit:
  return dev;
}

static int femodule_probe(struct usb_interface *interface,
                          const struct usb_device_id *id) {
  struct usb_femodule *dev = NULL;
  int retval = -ENODEV;

  DBG_INFO("Probing " FEMODULE_VARIANT_MCASE);

  dev = femodule_setup_dev(interface);
  if (!dev) {
    DBG_ERR("No supported endpoints on this interface");
    goto error;
  }
  if (dev->isoc_in_endpoint) {
    DBG_INFO("Found GNSS stream endpoint");
    init_waitqueue_head(&dev->gnss_output_queue.wqueue);
    const int size = bufferSizeForEndpoint(dev->isoc_in_endpoint);
    stuffing_ptr = kzalloc(size, GFP_KERNEL);
    if (!stuffing_ptr) {
      DBG_CRIT("Failed to allocate stuffing buffer");
      retval = -ENOMEM;
      goto error;
    }
    stuffing_size = size;
    get_random_bytes(stuffing_ptr, stuffing_size);
  }
  if (dev->int_in_endpoint) {
    DBG_INFO("Found IMU stream endpoint");
    init_waitqueue_head(&dev->imu_output_queue.wqueue);
    const int size = sizeof(struct femodule_reportimuconfig_struct);
    imuconfig_ptr = kzalloc(size, GFP_KERNEL);
    if (!imuconfig_ptr) {
      DBG_CRIT("Failed to allocate IMU configuration buffer");
      retval = -ENOMEM;
      goto error;
    }
    imuconfig_size = size;
    femodule_imuconfig_fill_initial();
  }

  /* We can register the device now, as it is ready. */
  retval = usb_register_dev(interface, &femodule_class);
  if (retval) {
    DBG_ERR("Not able to get a minor for this device");
    usb_set_intfdata(interface, NULL);
    goto error;
  }

  dev->minor = interface->minor;

  /* Initialize circular buffer . */
  femodule_init_circular_buffers(dev);

  if (dev->isoc_in_endpoint) {
#ifdef __LION
    DBG_INFO("Programming MAX2771");
    /* Program Maxim 2771 */
    retval = femodule_program_max2771(dev);
    if (retval) goto deregister_error;
#else
    DBG_INFO("Programming MAX2769");
    /* Program Maxim 2769 */
    retval = femodule_program_max2769(dev);
    if (retval) goto deregister_error;

    DBG_INFO("Programming ADF4360");
    /* Program ADF4360 */
    retval = femodule_program_adf4360(dev);
    if (retval) goto deregister_error;
#endif

    DBG_INFO("GNSS stream attached to /dev/" FEMODULE_VARIANT_LCASE "%d",
             interface->minor - FEMODULE_MINOR_BASE);
  }
  if (dev->int_in_endpoint) {
    DBG_INFO("IMU stream attached to /dev/" FEMODULE_VARIANT_LCASE "%d",
             interface->minor - FEMODULE_MINOR_BASE);
  }

  return retval;

deregister_error:
  usb_deregister_dev(interface, &femodule_class);
error:
  if (stuffing_ptr) {
    kfree(stuffing_ptr);
    stuffing_ptr = (char *)NULL;
    stuffing_size = 0;
  }
  if (imuconfig_ptr) {
    kfree(imuconfig_ptr);
    imuconfig_ptr = (char *)NULL;
    imuconfig_size = 0;
  }
  femodule_cleanup_dev(dev);
  return retval;
}

static void femodule_disconnect(struct usb_interface *interface) {
  struct usb_femodule *dev;
  int minor;

  mutex_lock(&disconnect_mutex); /* Not interruptible */

  dev = usb_get_intfdata(interface);

#ifdef __DEBUG_PROTECT
  if (!dev) {
    DBG_DEBUG("dev is NULL");
    return;
  }
#endif

  mutex_lock(&dev->mutex); /* Not interruptible */
  atomic_set(&dev->running, 0);

  usb_set_intfdata(interface, NULL);

  minor = dev->minor;

  /* Give back our minor. */
  usb_deregister_dev(interface, &femodule_class);

  if (dev->open) {
    dev->udev = NULL;
    femodule_stop_readers();
  } else {
    // Otherwise, if the device is not open, we can clean up right now
    femodule_cleanup_dev(dev);
  }

  mutex_unlock(&dev->mutex);
  mutex_unlock(&disconnect_mutex);

  DBG_INFO("Device /dev/" FEMODULE_VARIANT_LCASE "%d now disconnected",
           minor - FEMODULE_MINOR_BASE);
}

static struct usb_driver femodule_driver = {
    .name = "femodule",
    .id_table = femodule_table,
    .probe = femodule_probe,
    .disconnect = femodule_disconnect,
};

static int __init usb_femodule_init(void) {
  int result;

  mutex_init(&disconnect_mutex);

  result = usb_register(&femodule_driver);
  if (result) {
    DBG_ERR("Failed to register " FEMODULE_VARIANT_MCASE " driver");
  } else {
    DBG_INFO("Registered " FEMODULE_VARIANT_MCASE " driver");
  }

  return result;
}

static void __exit usb_femodule_exit(void) {
  usb_deregister(&femodule_driver);
  DBG_INFO("De-registered " FEMODULE_VARIANT_MCASE " driver");
}

module_init(usb_femodule_init);
module_exit(usb_femodule_exit);

MODULE_AUTHOR("Matthew James Murrian");
MODULE_LICENSE("GPL");
