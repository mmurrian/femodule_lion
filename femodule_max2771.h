// femodule_max2771.h
//
// Copyright (c) 2021 Matthew James Murrian. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#ifndef __LION
#error MAX2771 is only on the RadioLion
#endif

// RadioLion will clock in MS bit first of the first byte

// MAX2771 expects MSB first of the overall register:
// Therefore, the first byte sent must be that one containing the
// most-significant bit

// Address: 0x0
static const struct MAX2771_CONF_ONE_t {
  unsigned FGAIN : 1;
  unsigned FCENX : 1;
  unsigned F3OR5 : 1;
  unsigned FBW : 3;
  unsigned FCEN : 7;
  unsigned MIXERMODE : 2;
  unsigned LNAMODE : 2;
  unsigned MIXPOLE : 1;
  const unsigned reserved1 : 2;
  const unsigned reserved2 : 2;
  const unsigned reserved3 : 4;
  const unsigned reserved4 : 4;
  unsigned IDLE : 1;
  unsigned CHIPEN : 1;
} MAX2771_CONF_ONE_default = {
  FGAIN : 0x1,
  FCENX : 0x1,
  FCEN : 0x58,
  reserved1 : 0x1,
  reserved2 : 0x2,
  reserved3 : 0b1010,
  reserved4 : 0b1111,
  CHIPEN : 0x1
};

// Address: 0x1
static const struct MAX2771_CONF_TWO_t {
  const unsigned DIEID : 2;
  const unsigned reserved1 : 1;
  const unsigned reserved2 : 1;
  unsigned DRVCFG : 2;
  unsigned BITS : 3;
  unsigned FORMAT : 2;
  unsigned AGCMODE : 2;
  unsigned SPI_SDIO_CONFIG : 2;
  unsigned GAINREF : 12;
  unsigned IQEN : 1;
  unsigned ANAIMON : 1;
  const unsigned reserved3 : 2;
  const unsigned reserved4 : 1;
} MAX2771_CONF_TWO_default =
    {reserved2 : 0x1, BITS : 0x2, FORMAT : 0x1, GAINREF : 170, reserved3 : 0x1};

// Address: 0x2
static const struct MAX2771_CONF_THREE_t {
  unsigned STRMRST : 1;
  unsigned DATASYNCEN : 1;
  unsigned TIMESYNCEN : 1;
  unsigned STAMPEN : 1;
  unsigned STRMBITS : 2;
  const unsigned reserved1 : 3;
  unsigned STRMSTOP : 1;
  unsigned STRMSTART : 1;
  unsigned STRMEN : 1;
  unsigned PGAQEN : 1;
  unsigned PGAIEN : 1;
  const unsigned reserved2 : 1;
  unsigned FHIPEN : 1;
  const unsigned reserved3 : 1;
  const unsigned reserved4 : 1;
  const unsigned reserved5 : 1;
  const unsigned reserved6 : 1;
  unsigned HILOADEN : 1;
  const unsigned reserved7 : 1;
  unsigned GAININ : 6;
  const unsigned reserved8 : 4;
} MAX2771_CONF_THREE_default = {
  TIMESYNCEN : 0x1,
  STAMPEN : 0x1,
  STRMBITS : 0x1,
  reserved1 : 0x7,
  PGAIEN : 0x1,
  FHIPEN : 0x1,
  reserved3 : 0x1,
  reserved4 : 0x1,
  reserved5 : 0x1,
  reserved6 : 0x1,
  reserved7 : 0x1,
  GAININ : 0x3A
};

// Address: 0x3
static const struct MAX2771_PLL_CONF_t {
  const unsigned reserved1 : 1;
  const unsigned reserved2 : 1;
  unsigned PWRSAV : 1;
  unsigned INT_PLL : 1;
  const unsigned reserved3 : 3;
  const unsigned reserved4 : 1;
  const unsigned reserved5 : 1;
  unsigned ICP : 1;
  const unsigned reserved6 : 3;
  const unsigned reserved7 : 1;
  const unsigned reserved8 : 5;
  unsigned IXTAL : 2;
  const unsigned reserved9 : 2;
  const unsigned reserved10 : 1;
  unsigned REFOUTEN : 1;
  const unsigned reserved11 : 1;
  const unsigned reserved12 : 1;
  const unsigned reserved13 : 1;
  unsigned LOBAND : 1;
  unsigned REFDIV : 3;
} MAX2771_PLL_CONF_default = {
  INT_PLL : 0x1,
  reserved8 : 0x10,
  IXTAL : 0x1,
  reserved10 : 0x1,
  REFOUTEN : 0x1,
  reserved13 : 0x1,
  REFDIV : 0x3
};

// Address: 0x4
static const struct MAX2771_N_R_DIV_t {
  const unsigned reserved1 : 3;
  unsigned RDIV : 10;
  unsigned NDIV : 15;
  const unsigned reserved2 : 4;
} MAX2771_N_R_DIV_default = {RDIV : 16, NDIV : 1536};

// Address: 0x5
static const struct MAX2771_F_DIV_t {
  const unsigned reserved1 : 1;
  const unsigned reserved2 : 1;
  const unsigned reserved3 : 1;
  const unsigned reserved4 : 1;
  unsigned reserved5 : 4;
  unsigned FDIV : 20;
  unsigned reserved6 : 4;
} MAX2771_F_DIV_default = {reserved5 : 0x7, FDIV : 0x80000};

// Address: 0x6
static const struct MAX2771_RESERVED_t {
  const unsigned reserved1 : 28;
  const unsigned reserved2 : 4;
} MAX2771_RESERVED_default = {reserved1 : 0x8000000};

// Address: 0x7
static const struct MAX2771_CLK_ONE_t {
  unsigned MODE : 1;
  const unsigned reserved1 : 1;
  unsigned ADCCLK : 1;
  unsigned FCLKIN : 1;
  unsigned REFCLK_M_CNT : 12;
  unsigned REFCLK_L_CNT : 12;
  unsigned EXTADCCLK : 1;
  const unsigned reserved2 : 3;
} MAX2771_CLK_ONE_default = {
  reserved1 : 0x1,
  REFCLK_M_CNT : 1563,
  REFCLK_L_CNT : 256
};

// Address: 0x8
static const struct MAX2771_TEST_ONE_t {
  const unsigned reserved1 : 1;
  const unsigned reserved2 : 1;
  const unsigned reserved3 : 1;
  const unsigned reserved4 : 1;
  const unsigned reserved5 : 1;
  const unsigned reserved6 : 1;
  const unsigned reserved7 : 5;
  const unsigned reserved8 : 1;
  const unsigned reserved9 : 4;
  const unsigned reserved10 : 4;
  const unsigned reserved11 : 4;
  const unsigned reserved12 : 4;
  const unsigned reserved13 : 2;
  const unsigned reserved14 : 1;
  const unsigned reserved15 : 1;
} MAX2771_TEST_ONE_default = {
  reserved1 : 0x1,
  reserved7 : 0x10,
  reserved9 : 0xF,
  reserved11 : 0xE,
  reserved12 : 0x1
};

// Address: 0x9
static const struct MAX2771_TEST_TWO_t {
  const unsigned reserved1 : 1;
  const unsigned reserved2 : 1;
  const unsigned reserved3 : 1;
  const unsigned reserved4 : 5;
  const unsigned reserved5 : 5;
  const unsigned reserved6 : 1;
  const unsigned reserved7 : 1;
  const unsigned reserved8 : 1;
  const unsigned reserved9 : 1;
  const unsigned reserved10 : 1;
  const unsigned reserved11 : 2;
  const unsigned reserved12 : 1;
  const unsigned reserved13 : 1;
  const unsigned reserved14 : 3;
  const unsigned reserved15 : 4;
  const unsigned RD_CALC : 1;
  const unsigned reserved16 : 1;
  const unsigned reserved17 : 1;
} MAX2771_TEST_TWO_default = {reserved2 : 0x1, reserved14 : 0b011};

// Address: 0xA
static const struct MAX2771_CLK_TWO_t {
  const unsigned reserved1 : 2;
  unsigned CLKOUT_SEL : 1;
  unsigned PREFRACDIV_SEL : 1;
  unsigned ADCCLK_M_CNT : 12;
  unsigned ADCCLK_L_CNT : 12;
  const unsigned reserved2 : 1;
  const unsigned reserved3 : 3;
} MAX2771_CLK_TWO_default = {ADCCLK_M_CNT : 1563, ADCCLK_L_CNT : 256};

#define MAX2771_CONFIG_SIZE 11