// femodule.h
//
// Copyright (c) 2016-2021 The GRID Software Project. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#ifndef FEMODULE_H_
#define FEMODULE_H_

#if defined(__LYNX)
#define FEMODULE_VARIANT_UCASE "RADIOLYNX"
#define FEMODULE_VARIANT_MCASE "RadioLynx"
#define FEMODULE_VARIANT_LCASE "radiolynx"
#elif defined(__LION)
#define FEMODULE_VARIANT_UCASE "RADIOLION"
#define FEMODULE_VARIANT_MCASE "RadioLion"
#define FEMODULE_VARIANT_LCASE "radiolion"
#endif

#include <linux/atomic.h>
#include <linux/dma-mapping.h>
#include <linux/init.h>
#include <linux/ioctl.h>
#include <linux/module.h>
#include <linux/mutex.h>  /* mutexes */
#include <linux/random.h> /* get_random_bytes() */
#include <linux/slab.h>   /* kmalloc() */
#include <linux/usb.h>    /* USB stuff */
#include <linux/version.h>
#include <linux/wait.h>
#include <linux/workqueue.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 10, 0)
#include <linux/uaccess.h> /* copy_*_user */
#else
#include <asm/uaccess.h> /* copy_*_user */
#endif

#define DEBUG_LEVEL_DEBUG 0x1F
#define DEBUG_LEVEL_INFO 0x0F
#define DEBUG_LEVEL_WARN 0x07
#define DEBUG_LEVEL_ERROR 0x03
#define DEBUG_LEVEL_CRITICAL 0x01

#define DBG_DEBUG(fmt, args...)                               \
  if ((debug_level & DEBUG_LEVEL_DEBUG) == DEBUG_LEVEL_DEBUG) \
  printk(KERN_DEBUG "[debug] %s(%d): " fmt "\n", __FUNCTION__, __LINE__, ##args)
#define DBG_INFO(fmt, args...)                              \
  if ((debug_level & DEBUG_LEVEL_INFO) == DEBUG_LEVEL_INFO) \
  printk(KERN_DEBUG "[info]  %s(%d): " fmt "\n", __FUNCTION__, __LINE__, ##args)
#define DBG_WARN(fmt, args...)                              \
  if ((debug_level & DEBUG_LEVEL_WARN) == DEBUG_LEVEL_WARN) \
  printk(KERN_DEBUG "[warn]  %s(%d): " fmt "\n", __FUNCTION__, __LINE__, ##args)
#define DBG_ERR(fmt, args...)                                 \
  if ((debug_level & DEBUG_LEVEL_ERROR) == DEBUG_LEVEL_ERROR) \
  printk(KERN_DEBUG "[err]   %s(%d): " fmt "\n", __FUNCTION__, __LINE__, ##args)
#define DBG_CRIT(fmt, args...)                                      \
  if ((debug_level & DEBUG_LEVEL_CRITICAL) == DEBUG_LEVEL_CRITICAL) \
  printk(KERN_DEBUG "[crit]  %s(%d): " fmt "\n", __FUNCTION__, __LINE__, ##args)

struct cb_element {
  char *ptr; /* Virtual-pointer to an allocated buffer.  This is
              * usable directly by the driver to access the buffer.
              * This should always be NULL when a buffer is not
              * allocated.
              */
  int count; /* Byte-count of allocated buffer.  This should be 0
              * when a buffer is not allocated.
              */
};

struct cb_top {
  struct cb_element *cbuffer;
  size_t size;
  atomic_t idx;
};

struct oq_top {
  wait_queue_head_t wqueue;
  struct cb_element *oqueue;
  size_t size;
  atomic_t input_idx;
  atomic_t output_idx;
  atomic_t overrun;
  size_t output_oft;
  u64 tIndex;
};

struct femodule_transfer {
  struct work_struct work;
  struct urb *urb;
  struct usb_femodule *dev;
};

struct usb_femodule {
  struct mutex mutex;

  struct usb_device *udev;
  struct usb_interface *interface;
  unsigned char minor;

  bool open;

  struct usb_endpoint_descriptor *isoc_in_endpoint;
  struct femodule_transfer *isoc_in_transfer;
  unsigned int isoc_in_transfer_count;

  struct usb_endpoint_descriptor *int_in_endpoint;
  struct femodule_transfer *int_in_transfer;
  unsigned int int_in_transfer_count;

  struct workqueue_struct *workqueue;

  atomic_t running;
  int read_failure_cached;

  struct cb_top gnss_circular_buffer;
  struct oq_top gnss_output_queue;
  struct cb_top imu_circular_buffer;
  struct oq_top imu_output_queue;

  struct mutex mutex_isoc_in_data;
  int isoc_in_frame_errors, isoc_in_frame_empties;
  unsigned char isoc_in_timestamp[2];
  bool isoc_in_timestamp_valid;
  u64 isoc_in_tindex;
  int isoc_in_buffer_discard;

  struct mutex mutex_int_in_data;
  int int_in_frame_errors;
  u32 int_in_last_config;
};

struct usb_femodule_shared {
  struct usb_femodule dev_gnss;
  struct usb_femodule dev_imu;
};

#endif /* FEMODULE_H_ */
