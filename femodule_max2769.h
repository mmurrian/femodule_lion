// femodule_max2769.h
//
// Copyright (c) 2016-2021 The GRID Software Project. All rights reserved.
//
// SPDX-License-Identifier: GPL-2.0

#ifndef __LYNX
#error MAX2769 is only on the RadioLynx
#endif

#define MAX2769_LNAMODE 0b11  // Both LNAs inactive
#define MAX2769_FORMAT 0b01
#define MAX2769_IQEN 0

#ifdef __LYNX_EXT_CLK_10MHZ
#define MAX2769_REFDIV 0b11
#define MAX2769_RDIV 0b0000011000
#define MAX2769_NDIV 0b00111010111111
#else
#define MAX2769_REFDIV 0b10
#define MAX2769_RDIV 0b0000001110
#define MAX2769_NDIV 0b000010001111011
#endif

static const struct {
  const int addr : 4;
  int FGAIN : 1;
  int FCENX : 1;
  int F3OR5 : 1;
  int FBW : 2;
  int FCEN : 6;
  int ANTEN : 1;
  int MIXEN : 1;
  int LNAMODE : 2;
  int MIXPOLE : 1;
  int IMIX : 2;
  int ILO : 2;
  int ILNA2 : 2;
  int ILNA1 : 4;
  int IDLE : 1;
  int CHIPEN : 1;
} max2769_CONF_ONE = {
    0b0000,           // Address
    0b1,              // FGAIN
    0b1,              // FCENX
    0b0,              // F3OR5
    0b10,             // FBW
    0b100111,         // FCEN
    1,                // ANTEN
    1,                // MIXEN
    MAX2769_LNAMODE,  // LNAMODE
    0b0,              // MIXPOLE
    0b01,             // IMIX
    0b10,             // ILO
    0b11,             // ILNA2
    0b1111,           // ILNA1
    0,                // IDLE
    1                 // CHIPEN
};

static const struct {
  const int addr : 4;
  int DIEID : 2;
  int reserved1 : 1;
  int LOEN : 1;
  int DRVCFG : 2;
  int BITS : 3;
  int FORMAT : 2;
  int AGCMODE : 2;
  int reserved2 : 2;
  int GAINREF : 12;
  int IQEN : 1;
} max2769_CONF_TWO = {
    0b0001,          // Address
    0b00,            // DIEID
    0,               // RESERVED
    1,               // LOEN
    0b00,            // DRVCFG
    0b010,           // BITS
    MAX2769_FORMAT,  // FORMAT
    0b00,            // AGCMODE
    0b00,            // RESERVED
    0b000010101010,  // GAINREF
    MAX2769_IQEN     // IQEN
};

static const struct {
  const int addr : 4;
  int STRMRST : 1;
  int DATASYNCEN : 1;
  int TIMESYNCEN : 1;
  int STAMPEN : 1;
  int STRMBITS : 2;
  int STRMCOUNT : 3;
  int STRMSTOP : 1;
  int STRMSTART : 1;
  int STRMEN : 1;
  int PGAQEN : 1;
  int PGAIEN : 1;
  int reserved1 : 1;
  int reserved2 : 7;
  int GAININ : 6;
} max2769_CONF_THREE = {
    0b0010,  // Address
    0b0,     // STRMRST
    0,       // DATASYNCEN
    1,       // TIMESYNCEN
    1,       // STAMPEN
    0b01,    // STRMBITS
    0b111,   // STRMCOUNT
    0b0,     // STRMSTOP
    0b0,     // STRMSTART
    0b0,     // STRMEN
    0b1,     // PGAQEN
    0b1,     // PGAIEN
    0b1,     0b1011111,
    0b111010  // GAININ
};

static const struct {
  const int addr : 4;
  int reserved1 : 1;
  int reserved2 : 1;
  int PWRSAV : 1;
  int INT_PLL : 1;
  int CPTEST : 3;
  int reserved3 : 1;
  int PFDEN : 1;
  int ICP : 1;
  int LDMUX : 4;
  int XTALCAP : 5;
  int IXTAL : 2;
  int REFDIV : 2;
  int reserved4 : 1;
  int REFOUTEN : 1;
  int reserved5 : 1;
  int IVCO : 1;
  int VCOEN : 1;
} max2769_PLL_CONF = {
    0b0011,  // Address
    0b0,
    0b0,
    0b0,    // PWRSAV
    0b1,    // INT_POLL
    0b000,  // CPTEST
    0b0,
    0,               // PFDEN
    0b0,             // ICP
    0b0000,          // LDMUX
    0b10000,         // XTALCAP
    0b01,            // IXTAL
    MAX2769_REFDIV,  // REFDIV
    0b1,
    1,  // REFOUTEN
    0b0,
    0b0,  // IVCO
    1     // VCOEN
};

static const struct {
  const int addr : 4;
  int reserved : 3;
  int RDIV : 10;
  int NDIV : 15;
} max2769_N_R_DIV = {
    0b0100,  // Address
    0b000,
    MAX2769_RDIV,  // RDIV
    MAX2769_NDIV   // NDIV
};

static const struct {
  const int addr : 4;
  int reserved : 8;
  int FDIV : 20;
} max2769_F_DIV = {
    0b0101,  // Address
    0b01110000,
    0b10000000000000000000  // FDIV
};

static const struct {
  const int addr : 4;
  int FRAMECOUNT : 28;
} max2769_STRM = {
    0b0110,                         // Address
    0b1000000000000000000000000000  // FRAMECOUNT
};

static const struct {
  const int addr : 4;
  int MODE : 1;
  int SERCLK : 1;
  int ADCCLK : 1;
  int FCLKIN : 1;
  int M_CNT : 12;
  int L_CNT : 12;
} max2769_CLK = {
    0b0111,          // Address
    0b0,             // MODE
    0b1,             // SERCLK
    0b0,             // ADCCLK
    0b0,             // FCLKIN
    0b011000011011,  // M_CNT
    0b000100000000   // L_CNT
};

static const struct {
  const int addr : 4;
  int reserved : 28;
} max2769_TEST_ONE = {0b1000,  // Address
                      0b0001111000001111010000000001};

static const struct {
  const int addr : 4;
  int reserved : 28;
} max2769_TEST_TWO = {0b1001,  // Address
                      0b0001010011000000010000000010};

#define max2769_config_size 10
static const char *max2769_config[max2769_config_size] = {
    (char *)&max2769_CONF_ONE,   (char *)&max2769_CONF_TWO,
    (char *)&max2769_CONF_THREE, (char *)&max2769_PLL_CONF,
    (char *)&max2769_N_R_DIV,    (char *)&max2769_F_DIV,
    (char *)&max2769_STRM,       (char *)&max2769_CLK,
    (char *)&max2769_TEST_ONE,   (char *)&max2769_TEST_TWO};